<?php
// Heading
$_['heading_title']      	= 'Баннеры';

// Text
$_['text_success']          = 'Баннеры успешно обновлены!';
$_['text_list']          	= 'Список баннеров';
$_['text_add']          	= 'Добавление банера';
$_['text_edit']          	= 'Редактирование банера';
$_['text_default']          = 'По умолчанию';

// Column
$_['column_name']          	= 'Название баннера';
$_['column_status']         = 'Статус';
$_['column_action']         = 'Действие';

// Entry
$_['entry_name']          	= 'Название баннера:';
$_['entry_description_1']          	= 'Текст 1:';
$_['entry_description_2']          	= 'Текст 2:';
$_['entry_description_3']          	= 'Текст 3:';
$_['entry_btn_text']          	= 'Текст кнопки:';
$_['entry_link']          	= 'Ссылка:';
$_['entry_image']          	= 'Изображение:';
$_['entry_status']          = 'Статус:';
$_['entry_sort_order']      = 'Сортировка';
$_['entry_title']          	= 'Текст';


// Error
$_['error_permission']      = 'У Вас нет прав для изменения баннеров!';
$_['error_name']          	= 'Название баннера должно быть от 3 до 64 символов!';
$_['error_description_1']   = 'Текст должен быть от 2 до 9 символов!';
$_['error_description_2']   = 'Текст должен быть от 2 до 35 символов!';
$_['error_description_3']   = 'Текст должен быть от 2 до 68 символов!';
$_['error_btn_text']        = 'Текст должен быть от 2 до 38 символов!';
