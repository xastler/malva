<?php echo $header; ?>
<script    src="view/javascript/jquery/jquery.scrollTo-1.4.3.1-min.js"></script>
<script    src="view/javascript/jquery/jquery.highlighttextarea.min.js"></script>
<script    src="view/javascript/jquery/jquery.fixer.js"></script>
<style>
/*start highlighttextarea*/
.highlightTextarea{position:relative}.highlightTextarea .highlightTextarea-container{position:absolute;margin:0;overflow:hidden}.highlightTextarea .highlightTextarea-highlighter{position:relative;border:none;padding:0;margin:0;color:transparent;cursor:text;overflow:hidden;white-space:pre-wrap;word-wrap:break-word}.highlightTextarea.debug .highlightTextarea-highlighter{color:red;border:1px solid red;margin:-1px}.highlightTextarea mark{line-height:inherit;color:transparent;margin:0;padding:0}.highlightTextarea input,.highlightTextarea textarea{position:absolute;left:0;top:0;resize:none;white-space:pre-wrap;word-wrap:break-word}.highlightTextarea .ui-wrapper{margin:0!important}.highlightTextarea .ui-resizable-se{bottom:15px;right:0}
/*end highlighttextarea*/

.clr {
	clear: both;
}

.filelistall {
	float: left;
	margin: 4px 5px 0 0;
	cursor: pointer;
}

.t_lang td input {
	width: 200px;
	margin: 5px 0;
}

input, textarea {
	outline: none;
}

.t_lang tr:hover td input, .t_lang tr:hover td textarea {
	background: #f5f5f5;
}

.t_lang tr td input:focus, .t_lang tr td textarea:focus {
	box-shadow: 0 0 5px rgba(47, 197, 240, 1);
	border: 1px solid rgba(47, 197, 240, 0.8);
}

.show_hide {
	clear: both;
	margin: 10px 0 10px 35px;
	font-size: 18px;
}

.show_hide a {
	cursor: pointer;
}

.show_hide_text {
	display: block;
	padding: 11px 0 0 0;
}

.filelist {
	float: left;
	margin: 4px 5px 0 0
}

.folders_block {
	clear: both;
	border: 1px solid #ccc;
	display: none;
	margin: 20px 20px 30px 35px;
	padding: 10px
}

.files_block {
	border: 1px dotted #ccc;
	margin: 5px 20px 20px 15px;
	padding: 10px;
	position: relative;
}

.files_block .button_add_var {
	float: left;
	display: block;
	background-color: #1EC2CF;
	border-color: #19A5AB;
	padding: 4px 8px;
}

.files_block .button_add_var i {
	color: #fff;
}

.files_block .buttons-left{float:left;}
.files_block .buttons-right{
	position:absolute;bottom:0;right:0;margin:20px;
}

.files_block .button_save_lang, .files_block .button_save_lang_one {
	float: right;
	display: block;
	background: #1e91cf;
	background-color: #1e91cf;
	border-color: #1978ab;
}

.files_block .button_save_lang i,.files_block .button_save_lang_one i {
	color: #fff;
}

.t_lang td input.new_value {
	font-size: 10px;
	width: 100px;
	font-weight: bold;
}

.s {
	border-color: #BE2026;
}

.save_time {
	display: none;
	float: right;
	font-weight: bold;
	margin: 5px 10px 0 0;
}

td input {
	width: 200px;
	border: 1px solid #abadb3;
	padding: 1px;
}

td textarea {
	width: 202px;
	border: 1px solid #abadb3;
	padding: 1px;
}

.back_error {
	position: fixed;
	top: 273px;
	display: block;
	padding: 0px 5px 1px;
	text-align: center;
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
	color: #fff;
	background: #BE2026;
	border: 1px solid #BE2026;
	border-radius: 10px 10px 3px 3px;
	cursor: pointer;
}

.next_error {
	position: fixed;
	top: 297px;
	display: block;
	padding: 0px 5px 1px;
	text-align: center;
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
	color: #fff;
	background: #BE2026;
	border: 1px solid #BE2026;
	border-radius: 3px 3px 10px 10px;
	cursor: pointer;
}

.back_error:hover, .next_error:hover {
	color: #BE2026;
	background: #fff;
}

.lang_error {
	display: block;
	position: absolute;
	width: 10px;
	height: 10px;
	margin: 3px 0 0 -28px;
	background: #BE2026;
	border-radius: 10px;
}
.parseError{width:400px;height:400px;}
</style>
<?php echo $column_left; ?>



<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip"
					title="<?php echo $button_cancel; ?>" class="btn btn-default"><i
					class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
		</div>
	</div>
	<div class="container-fluid">
    <?php if (isset($error['error_warning'])) { ?>
    <div class="alert alert-danger">
			<i class="fa fa-exclamation-circle"></i> <?php echo $error['error_warning']; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
    <?php } ?>
    <div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">
				<h2 style="text-transform: uppercase;"><?php echo $entry_title; ?></h2>

<?php 
/*
        ?><textarea>
        <?php
        $f = $end_side . 'english/account/account.php';

		try {
		  if ( syntax_check_php_file($f) ) {
		    echo 'OK';
		  }
		}
		catch (Exception $e) {
		  echo $e->getMessage();
		}

        /*if (noParseErrors ( $f ))
        echo '@@@';
        else
        echo '###';
        ?>
        </textarea>
        <?php
       */
?>
				<span><a class="filelistall" title="<?php echo $entry_showhide; ?>"><?php echo $entry_showhideall; ?></a></span>
				<a class="back_error">˄</a> <a class="next_error">˅</a>
				<div class="clr">
					<script   >
			$(document).ready(function(){				
				var notext;
				$('.back_error').click(function() {
					if($('.lang_error:visible:eq(0)').length!=0){
						$('.lang_error:visible:eq(' + notext + ')').stop(true, true);
						$('.lang_error').css('opacity', '0.5');
						
						if ( notext === undefined) {
							notext=0;
						} else {
							notext=notext-1;
						}
						if($('.lang_error:visible:eq(' + notext + ')').length==0){
							notext=$('.lang_error:visible').length-1;
						}
						$('body').scrollTo($('.lang_error:visible:eq(' + notext + ')'), 200, {offset:-258, } );
						$('.lang_error:visible:eq(' + notext + ')').stop(true, true).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200);
					}else{
						$('body').scrollTo(0, 200);
					}
				});
				
				$('.next_error').click(function() {
					if($('.lang_error:visible:eq(0)').length!=0){
						$('.lang_error:visible:eq(' + notext + ')').stop(true, true);
						$('.lang_error').css('opacity', '0.5');
						
						if ( notext === undefined) {
							notext=0;
						} else {
							notext=notext+1;
						}
						if($('.lang_error:visible:eq(' + notext + ')').length==0){
							notext=0;
						}
						$('body').scrollTo($('.lang_error:visible:eq(' + notext + ')'), 200, {offset:-258, } );
						$('.lang_error:visible:eq(' + notext + ')').stop(true, true).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200);
					}else{
						$('body').scrollTo($(document).height(), 200);
					}
				});

				$('.lang_error').on('click', function(){
					notext = $(this).index('.lang_error:visible');

					$('.lang_error').css('opacity', '0.5');
					$('body').scrollTo($(this), 200, {offset:-258, } );
					$(this).stop(true, true).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200).animate({opacity: 0.2,}, 200).animate({opacity: 1,}, 200);
				});
			});
		</script>
        <?php
								$show_folder = '';
								
								$td_num = 0;
								foreach ( $paths_array as $path ) {
									$folder = explode ( '/', $path );
									if ($show_folder != $folder [0]) {
										$show_folder = $folder [0];
										?>
      			</div>
				<div class="show_hide">
					<a data-toggle="tooltip" title="<?php echo $entry_showhide; ?>"
						class="filelist btn btn-success"
						data-original-title="<?php echo $entry_showhide; ?>"><i
						class="fa fa-plus-circle"></i></a> <a class="show_hide_text"
						onclick="$(this).prev().click();"
						title="<?php echo $entry_showhide; ?>"><?php echo $show_folder; ?></a>
				</div>
				<div class="folders_block">
      		<?php } ?>
      		<div>
	      		<?php echo $folder [1]; ?>
	      		<div class="files_block">
							<form method="post" action="<?php echo $action; ?>">
								<input type="hidden" name="type" value="<?php echo $type; ?>" />
								<input type="hidden" name="path" value="<?php echo $path; ?>" />
								<table class="t_lang">
									<tr>
										<th></th>
						<?php $parseError = array (); ?>
						<?php foreach ($selected as $lang){ ?>
							<th><?php echo $lang; ?></th>
							<?php $parseError{$lang}=0;?>
						<?php } ?>
						<?php $parseErrorDis=0;?>
						</tr>
							<?php $input_val = getFilesByLang ( $path, $selected, $end_side, false ); ?>
							<?php $input_var = getFilesByLang ( $path, $selected, $end_side, true ); ?>
							
							<?php
								foreach ($input_var as $k => $v){
									foreach ($selected as $lang){
									if(isset($input_val{$lang}['parseError']))
										$parseErrorDis=1;
									}
								}
							?>
							
							<?php foreach ($input_var as $k => $v){ ?>
							<?php $td_num++; ?>
								<tr>
										<td class="tn<?php echo $td_num; ?>"><?php echo $v; ?></td>
									<?php
											$len = 0;
											foreach ( $selected as $lang ) {
												if (isset ( $input_val {$lang} {$v} ) && $len < mb_strlen ( htmlspecialchars ( $input_val {$lang} {$v} ), "UTF-8" ))
													$len = mb_strlen ( htmlspecialchars ( $input_val {$lang} {$v} ), "UTF-8" );
											}
											?>
									<?php $error_var = 0; ?>
					      			<?php foreach ($selected as $lang){ ?>
					      				<?php if($parseError{$lang}==0){ ?>
											<?php if(isset($input_val{$lang}['parseError'])) {?>
												<?php $parseError{$lang}=1;?>
										<td rowspan="<?php echo count($input_var);?>" style="width:1%;vertical-align:top;">
										<div style="padding:0 20px 0 0"><?php echo $input_val{$lang}['parseErrorText']; ?></div><br />
										<?php $fileCont = file_get_contents($input_val{$lang}['parseErrorFile']); ?>
										<?php $errorLineTxt = explode('on line ', $input_val{$lang}['parseErrorText']);?>
										<?php $errorLine = trim(strip_tags ($errorLineTxt[1]))-1;?>
										<?php $getFileError = explode("\n",$fileCont); ?>
										<?php $getFileErrorLine = $getFileError[$errorLine]; ?>
										<div class="parseError"><textarea class="parseError"><?php echo trim($fileCont); ?></textarea>
										<a data-toggle="tooltip" data-url="<?php echo str_replace($end_side,'',$input_val{$lang}['parseErrorFile']); ?>" title="<?php echo $button_save; ?>" class="btn button button_save_lang_one" style="margin-top:10px;"><i class="fa fa-save"></i></a>
										</div>
										<?php if(mb_strlen($getFileErrorLine)>0){ ?>
										<script   >
										$(document).ready(function(){
											/* $errorLine: <?php echo $errorLine;?>*/
											/* $getFileErrorLine: <?php print_r(explode("\n",$fileCont));?>*/
											/* $getFileErrorLine: <?php echo $getFileErrorLine;?>*/
											$('.parseError').highlightTextarea({
												ranges: [{
													  color: '#FFFF00',
													  start: <?php echo mb_strpos($fileCont,$getFileErrorLine)+2?>,
										              length: <?php echo mb_strlen($getFileErrorLine)+1; ?>
										          }]
											});
										});	
										</script>
										<?php } ?>
										</td>
											<?php }else{?>
										<td>
												<?php if(!isset($input_val{$lang}{$v}) && $error_var==0) { ?>
													<?php $error_var=1; ?>
													<script   >
														$(document).ready(function(){
															$('.tn<?php echo $td_num; ?>').prepend('<div class="lang_error"></div>');
														});
													</script>
												<?php } ?>
												
												<?php if($len > 25){ ?>
													<textarea name="<?php echo 'data[' . $lang . '][' . $v . ']'; ?>" rows="<?php echo ceil($len / 25);?>"<?php if(!isset($input_val{$lang}{$v})) echo ' class="s"'; ?><?php if($parseErrorDis==1) echo ' disabled="disabled"';?>><?php echo (isset($input_val{$lang}{$v}) ? htmlspecialchars($input_val{$lang}{$v}) : '') ?></textarea>
												<?php }else{ ?>
													<input type="text" name="<?php echo 'data[' . $lang . '][' . $v . ']'; ?>" value="<?php echo (isset($input_val{$lang}{$v}) ? htmlspecialchars($input_val{$lang}{$v}) : '') ?>"<?php if(!isset($input_val{$lang}{$v})) echo ' class="s"'; ?><?php if($parseErrorDis==1) echo ' disabled="disabled"';?> />
												<?php } ?>
										</td>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								</tr>
							<?php } ?>
					</table>
								<div class="buttons-left">
									<a class="btn button button_add_var"><i class="fa fa-plus-circle"></i></a>
								</div>
								<div class="buttons-right">
								<?php if($parseErrorDis==1) { ?>
									<button title="" type="button" class="btn button button_save_lang" disabled="disabled" style="display: inline-block;"><i class="fa fa-save"></i></button>
									<div style="display: inline-block;float: right;padding:10px 10px 0 0;color:#BE2026;font-weight:bold;">You must first fix Parse errors:</div>
								<?php }else{?>
									<a data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn button button_save_lang"><i class="fa fa-save"></i></a>
								<?php }?>
									<div class="save_time"></div>
								</div>
							</form>
							<div class="clr"></div>

						</div>
					</div>
      	<?php } ?>
      	</div>


				<div class="folders_block" style="display: block;">
					<div class="show_hide"><?php echo $entry_languages; ?></div>
					<div class="files_block">
						<form method="post" action="<?php echo $action; ?>">
							<input type="hidden" name="type" value="<?php echo $type; ?>" />
							<input type="hidden" name="path" value="#main_file" />
							<table class="t_lang">
								<tr>
									<th></th>
							<?php $parseError = array (); ?>
							<?php foreach ($selected as $lang){ ?>
								<th><?php echo $lang; ?></th>
								<?php $parseError{$lang}=0;?>
							<?php } ?>
							<?php $parseErrorDis=0;?>
							</tr>
							<?php $input_val = getFilesByLang('#main_file', $selected, $end_side, false); ?>
							<?php $input_var = getFilesByLang('#main_file', $selected, $end_side, true); ?>
							
							<?php
								foreach ($input_var as $k => $v){
									foreach ($selected as $lang){
									if(isset($input_val{$lang}['parseError']))
										$parseErrorDis=1;
									}
								}
							?>
							
							<?php foreach ($input_var as $k => $v){ ?>
							<?php $td_num++; ?>
								<tr>
									<td class="tn<?php echo $td_num; ?>"><?php echo $v; ?>:</td>
									<?php
										$len = 0;
										foreach ( $selected as $lang ) {
											if (isset ( $input_val {$lang} {$v} ) && $len < mb_strlen ( htmlspecialchars ( $input_val {$lang} {$v} ), "UTF-8" ))
												$len = mb_strlen ( htmlspecialchars ( $input_val {$lang} {$v} ), "UTF-8" );
										}
									?>
									<?php $error_var = 0; ?>
					      			<?php foreach ($selected as $lang){ ?>
					      				<?php if($parseError{$lang}==0){ ?>
											<?php if(isset($input_val{$lang}['parseError'])) {?>
												<?php $parseError{$lang}=1;?>
										<td rowspan="<?php echo count($input_var);?>" style="width:1%;vertical-align:top;">
										<div style="padding:0 20px 0 0"><?php echo $input_val{$lang}['parseErrorText']; ?></div><br />
										<?php $fileCont = file_get_contents($input_val{$lang}['parseErrorFile']); ?>
										<?php $errorLineTxt = explode('on line ', $input_val{$lang}['parseErrorText']);?>
										<?php $errorLine = trim(strip_tags ($errorLineTxt[1]))-1; ?>
										<?php $getFileError = explode("\n",$fileCont); ?>
										<?php $getFileErrorLine = $getFileError[$errorLine]; ?>
										<div class="parseError"><textarea class="parseError"><?php echo trim($fileCont); ?></textarea>
										<a data-toggle="tooltip" data-url="<?php echo str_replace($end_side,'',$input_val{$lang}['parseErrorFile']); ?>" title="<?php echo $button_save; ?>" class="btn button button_save_lang_one" style="margin-top:10px;"><i class="fa fa-save"></i></a>
										</div>
										<?php if(mb_strlen($getFileErrorLine)>0){ ?>
										<script   >
										$(document).ready(function(){
											/* $errorLine: <?php echo $errorLine;?>*/
											/* $getFileErrorLine: <?php print_r(explode("\n",$fileCont));?>*/
											/* $getFileErrorLine: <?php echo $getFileErrorLine;?>*/
											$('.parseError').highlightTextarea({
												ranges: [{
													  color: '#FFFF00',
													  start: <?php echo mb_strpos($fileCont,$getFileErrorLine)+2?>,
										              length: <?php echo mb_strlen($getFileErrorLine)+1; ?>
										          }]
											});
										});	
										</script>
										<?php } ?>
										</td>
											<?php }else{?>
										<td>
										
											<?php if(!isset($input_val{$lang}{$v}) && $error_var==0) { ?>
											<?php $error_var=1; ?>
												<script   >
													$(document).ready(function(){
														$('.tn<?php echo $td_num; ?>').prepend('<div class="lang_error"></div>');
													});
												</script>
											<?php } ?>
											
											<?php if($len > 25){ ?>
												<textarea name="<?php echo 'data[' . $lang . '][' . $v . ']'; ?>" rows="<?php echo ceil($len / 25);?>"<?php if(!isset($input_val{$lang}{$v})) echo ' class="s"'; ?>><?php echo (isset($input_val{$lang}{$v}) ? htmlspecialchars($input_val{$lang}{$v}) : '') ?></textarea>
											<?php }else{ ?>
												<input type="text" name="<?php echo 'data[' . $lang . '][' . $v . ']'; ?>" value="<?php echo (isset($input_val{$lang}{$v}) ? htmlspecialchars($input_val{$lang}{$v}) : '') ?>"<?php if(!isset($input_val{$lang}{$v})) echo ' class="s"'; ?> />
												<?php } ?>
										</td>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								</tr>
							<?php } ?>
					</table>
							<div class="buttons-left">
								<a class="btn button button_add_var"><i class="fa fa-plus-circle"></i></a>
							</div>
							<div class="buttons-right">
							<?php if($parseErrorDis==1) { ?>
								<button title="" type="button" class="btn button button_save_lang" disabled="disabled" style="display: inline-block;"><i class="fa fa-save"></i></button>
								<div style="display: inline-block;float: right;padding:10px 10px 0 0;color:#BE2026;font-weight:bold;">You must first fix Parse errors:</div>
							<?php }else{ ?>
								<a data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn button button_save_lang"><i class="fa fa-save"></i></a>
							<?php }?>
								<div class="save_time"></div>
							</div>
						</form>
						<div class="clr"></div>
					</div>
				</div>






			</div>
		</div>
	</div>
<?php

?>
<script   >
	$(document).ready(function(){
	    $('.buttons-right').fixer({
            gap: 20,
            gapRevers: 88
        });
		
		$('.button_add_var').click(function(){
			$(this).parent().prev().append("<tr><td><span>text_</span><br /><input type=\"text\" class=\"new_value\" value=\"text_\" /></td><?php foreach ($selected as $lang){ ?><td><textarea name=\"<?php echo 'data[' . $lang . '][-]'; ?>\" rows=\"3\" class=\"<?php echo $lang; ?>\"></textarea></td><?php } ?></tr>");
		});
		$('.files_block').on('keyup', '.new_value', function(){
			//alert($(this).val());
			$(this).prevAll('span').html($(this).val());
			<?php foreach ($selected as $lang){ ?>
			$(this).parent().nextAll().children('.<?php echo $lang; ?>').attr('name', 'data[<?php echo $lang; ?>][' + $(this).val() + ']');
			<?php } ?>
		});
		
		$('.button_save_lang').click(function(){
			var now = new Date();
			var hours = now.getHours();
			var minutes = now.getMinutes();
			var seconds = now.getSeconds();
			var timeValue = '<?php echo $entry_saved; ?>: ' + ((hours < 10) ? "0" : "") + hours;
			timeValue += ((minutes < 10) ? ":0" : ":") + minutes;
			timeValue += ((seconds < 10) ? ":0" : ":") + seconds;

			
			  var save_time = $(this).next('.save_time');
			  $.ajax({
			      type: 'POST',
			      url: $(this).parents('form').attr('action'),
			      data: $(this).parents('form').serialize(),
			      beforeSend: function() {
			    	  save_time.show();
			    	  save_time.html('<?php echo $entry_saving; ?>');
			      },
			      success: function(msg) {
				      if(msg == 'ok') {
					      save_time.fadeOut(500,function(){
					    	  save_time.html(timeValue);
					    	  save_time.fadeIn(500);
						  });
					  } else if(msg == 'error') {
					  	alert(msg);
		      		  } else {
			      		window.location.href = '<?php echo $redirectAmp?>';
		      		  }
			      }
			    });
		});

		
		$('.button_save_lang_one').click(function(){
			  $.ajax({
			      type: 'POST',
			      url: '<?php echo $actionOneFile; ?>',
			      data: {data: $(this).parent().children('div').children('textarea').val(), file: $(this).attr('data-url'), type: '<?php echo $type; ?>' },
			      success: function(msg) {
				      if(msg == 'ok') {
				    	  alert('<?php echo $entry_saved; ?>');
				    	  window.location.href = '<?php echo $redirectAmp?>';
					  } else if(msg == 'error') {
					  	alert(msg);
		      		  } else {
		      			alert(msg);
			      		window.location.href = '<?php echo $redirectAmp?>';
		      		  }
			      }
			    });
		});
		
		$('.filelistall').toggler(function(){
			$('.filelist').parent().next().show();
			$('.filelist').removeClass('btn-success').addClass('btn-danger');
			$('.filelist').children('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
		},function(){
			$('.filelist').parent().next().hide();
			$('.filelist').removeClass('btn-danger').addClass('btn-success');
			$('.filelist').children('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
		}
		);
				
		$('.filelist').click(function(){
			$(this).parent().next().toggle();
			if($(this).hasClass('btn-success')){
				$(this).removeClass('btn-success').addClass('btn-danger');
				$(this).children('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
			}else{
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).children('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
			}
		});

		$('.lang_error').css('opacity', '0.5');
		$('.lang_error').hover(function(){ $(this).css('opacity', '0.7'); },function(){ $(this).css('opacity', '0.5'); });
	});

	(function( $ ){
		  $.fn.toggler = function( fn, fn2 ) {
		    var args = arguments,guid = fn.guid || $.guid++,i=0,
		    toggler = function( event ) {
		      var lastToggle = ( $._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
		      $._data( this, "lastToggle" + fn.guid, lastToggle + 1 );
		      event.preventDefault();
		      return args[ lastToggle ].apply( this, arguments ) || false;
		    };
		    toggler.guid = guid;
		    while ( i < args.length ) {
		      args[ i++ ].guid = guid;
		    }
		    return this.click( toggler );
		  };
		})( jQuery );
	
</script>
<?php

function syntax_check_php_file ($file) {
	@$code = file_get_contents($file);
	 
	if ($code === false) {
		throw new Exception('File '.$file.' does not exist');
	}
	 
	$braces = 0;
	$inString = 0;
	foreach ( token_get_all($code) as $token ) {
		if ( is_array($token) ) {
			switch ($token[0]) {
				case T_CURLY_OPEN:
				case T_DOLLAR_OPEN_CURLY_BRACES:
				case T_START_HEREDOC: ++$inString; break;
				case T_END_HEREDOC:   --$inString; break;
			}
		}
		else if ($inString & 1) {
			switch ($token) {
				case '`':
				case '"': --$inString; break;
			}
		}
		else {
			switch ($token) {
				case '`':
				case '"': ++$inString; break;

				case '{': ++$braces; break;
				case '}':
					if ($inString) {
						--$inString;
					}
					else {
						--$braces;
						if ($braces < 0) {
							throw new Exception('Braces problem!');
						}
					}
					break;
			}
		}
	}
	 
	//if ($braces) {
	//	throw new Exception('Braces problem!');
	//}
	 
	$res = false;
	$codeByLineAr = array_filter(explode("\n",$code));
	$codeByLine = substr(trim(end($codeByLineAr)), -2);
	 
	ob_start();

	$res = eval('if (0) {?>' . $code . (($codeByLine=='?>') ? '<?php' : '') . ' }; return true;');
	$error_text = ob_get_clean();
	 
	header('HTTP/1.0 200 OK');
	 
	if (!$res) {
		throw new Exception($error_text);
	}
	 
	return true;
}



function getFilesByLang($file, $lang_array, $end_side, $vars) {
	// $vars get variables=true | get values=false
	ob_start ();
	$list = array ();
	foreach ( $lang_array as $lang ) {
		$parseError = false;
		if ($file == '#main_file') {
			$fileName = $end_side . $lang . '/' . $lang . '.php';
			if (file_exists ( $fileName )) {
				try{
					if (syntax_check_php_file ( $fileName )) {
						include $fileName;
					}
				} catch (Exception $e) {
					$parseError = true;
					$parseErrorFile = $fileName;
					$parseErrorText = $e->getMessage();
				}
			} else {
				$fileName = $end_side . $lang . '/default.php';
				if (file_exists ( $fileName )) {
					try{
						if (syntax_check_php_file ( $fileName )) {
							include $fileName;
						}
					} catch (Exception $e) {
						$parseError = true;
						$parseErrorFile = $fileName;
						$parseErrorText = $e->getMessage();
					}
				}
			}
		} else {
			$fileName = $end_side . $lang . '/' . $file;
			if (file_exists ( $fileName )) {
				try{
					if (syntax_check_php_file ( $fileName )) {
						include $fileName;
					}
				} catch (Exception $e) {
					$parseError = true;
					$parseErrorFile = $fileName;
					$parseErrorText = $e->getMessage();
				}
			}
		}
		$gdv = get_defined_vars ();
		if ($parseError && !$vars){
			$list {$lang} ['parseError'] = $parseError;
			$list {$lang} ['parseErrorFile'] = $parseErrorFile;
			$list {$lang} ['parseErrorText'] = $parseErrorText;
		}
		
		if (isset ( $gdv ['_'] )) {
			if (is_array ( $gdv ['_'] )) {
				foreach ( $gdv ['_'] as $k => $v ) {
					if ($vars) {
						$list [] = $k;
					} else {
						$list {$lang} {$k} = $v;
						unset ( $_ {$k} );
					}
				}
			}
		}
	}
	ob_end_clean ();
	
	if($vars)
		return array_unique ( $list );
	return $list;
}
?>