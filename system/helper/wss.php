<?php
    if (!function_exists('wss_log')) {
        function wss_log($message,$file_name = 'wss_log',$file_type='log'){
            $file = DIR_LOGS . $file_name.'.'.$file_type;
            $handle = fopen($file, 'a');

            flock($handle, LOCK_EX);
            if(is_array($message)){
                fwrite($handle, date('Y-m-d G:i:s') . "\n" .print_r($message, true). "\n");
            }else{
                fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message. "\n");
            }
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

?>
