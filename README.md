# README #

demo:
http://ocstore.progekt.com.ua/

login: admin
password: admin

### What is this repository for? ###

* OcStore 2.1.0.2.1


### модули ###
 
* SeoPro
* News / Blog V4.4 - V4.5
* Simple v 4.6.9
* Купить в один клик (с фиксацией заказа)
* Easy Language Editor (редактирование языковых файлов)
* Mega Filter PRO (2.0.4.3)
* CSV Price Pro import/export 4
(гератор ключа для домен - http://feth.ru/csvpricepro_noindex)

### Модификации SEO  ###
* Три языка (Укр, Анг, рус)
* Герация URL  при создании и редактировани
* Schema.org breadcrumbs
* Ogg теги
* Prev Next Pagination
* Meta title, h1, keyword, description

