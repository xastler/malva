<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Домашній текстиль</title>
    <base href="http://malva/" />
    <meta property="og:title" content="Домашній текстиль" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://malva/ru/domashniy-tekstil-60/page-3" />
    <meta property="og:image" content="http://malva/image/cache/catalog/categoru/pic-catalog-755x500.png" />
    <meta property="og:site_name" content="Malva" />

    <meta property="url" content="http://malva/ru/domashniy-tekstil-60/page-3" />
    <meta property="title" content="Домашній текстиль" />
    <meta property="image" content="http://malva/image/cache/catalog/categoru/pic-catalog-755x500.png" />

    <meta itemprop="url" content="http://malva/ru/domashniy-tekstil-60/page-3">
    <meta itemprop="name" content="Домашній текстиль" />
    <meta itemprop="image" content="http://malva/image/cache/catalog/categoru/pic-catalog-755x500.png">

    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"   ></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/mf/jquery-ui.min.js"   ></script>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"   ></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/icomoon-eff/style.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/fonts/fonts.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.css" rel="stylesheet" />
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.theme.css" rel="stylesheet" />
    <script src="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.min.js"></script>
    <script src="catalog/view/javascript/social_auth.js"   ></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEi_mKbOqvmIL0qXYZEwCZujvacMpBxGQ&callback" ></script>
    <link href="catalog/view/theme/default/stylesheet/mf/jquery-ui.min.css?v2.0.4.3" type="text/css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/mf/style.css?v2.0.4.3" type="text/css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/mf/style-2.css?v2.0.4.3" type="text/css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
    <script src="catalog/view/javascript/common.js"   ></script>
    <script src="catalog/view/javascript/main.js"   ></script>
    <link href="http://malva/ru/domashniy-tekstil-60/page-2" rel="prev" />
    <link href="http://malva/image/catalog/group-2.png" rel="icon" />
    <script src="catalog/view/javascript/mf/iscroll.js?v2.0.4.3"   ></script>
    <script src="catalog/view/javascript/mf/mega_filter.js?v2.0.4.3"   ></script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": "http://malva/",
            "logo": "http://malva/image/catalog/logo-fin.png"
        }
    </script>

    <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js"   ></script>
    <link href="catalog/view/javascript/popup_purchase/magnific-popup.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />
    <script   >
        $(function() {
            $.each($("[onclick^='cart.add']"), function() {
                var product_id = $(this).attr('onclick').match(/[0-9]+/);
                $(this).parent().before("<div class='button-group popup-purchase-button'><button>Купить в 1 клик</button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
            });
            $.each($(".product-thumb [onclick^='get_popup_cart']"), function() {
                var product_id = $(this).attr('onclick').match(/[0-9]+/);
                $(this).parent().before("<div class='button-group popup-purchase-button'><button>Купить в 1 клик</button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
            });
            var main_product_id = $('input[name=\'product_id\']').val();
            $('#button-cart').before( "<button class='btn btn-min-st1'>Купить в 1 клик</button>" ).prev().attr('onclick', 'get_popup_purchase(\'' + main_product_id + '\');');
        });
        function get_popup_purchase(product_id) {
            $.magnificPopup.open({
                tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
                items: {
                    src: 'index.php?route=module/popup_purchase&product_id='+product_id,
                    type: 'ajax'
                }
            });
        }
    </script>

</head>
<body class="product-category-60 category-page">

<div class="body_inst">
    <a href="https://www.instagram.com/DREAMSTEXTILE/" rel="nofollow noopener" target="_blank"><i class="icon-instagram"></i><span>#dreamstextile</span></a>
</div>
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2">
                <div id="logo">
                    <a href="http://malva/ru/"><img src="http://malva/image/catalog/logo-fin.png" title="Malva" alt="Malva" class="img-responsive" /></a>
                </div>
            </div>
            <div class="col-lg-10 col-md-10">
                <div class="header-top">
                    <div class="header-top-info">
                        <a href="tel:+380680000001" class="header-top-tel">+38 (068) 000-00-01</a>                        <a href="mail:malvakm@gmail.com.ua">malvakm@gmail.com.ua</a>                    </div>
                    <div class="header-top-navigation">
                        <div id="search" class="search">
                            <input type="text" name="search" value="" placeholder="Поиск" class="form-control" />
                            <button type="button"><i class="icon-magnifying-glass"></i></button>
                        </div>                        <a href="http://malva/ru/wishlist/" id="wishlist-total" ><i class="icon-heart"></i><span>0</span></a>
                        <a id="cart" href="http://malva/ru/index.php?route=checkout/simplecheckout"><i class="icon-shopping-bag"></i><span>0</span></a>

                        <a href="javascript:void(0);" title=" " class="quick_login"><i class="icon-user"></i></a>

                        <form action="http://malva/ru/index.php?route=common/language/language" method="post" enctype="multipart/form-data" id="language">
                            <button class="btn-language" data-toggle="dropdown">
                                <span class="">RU</span> <i class="icon-down-arrow"></i></button>
                            <ul class="dropdown-menu">
                                <li><a href="ru">ru</a></li>
                                <li><a href="ua">ua</a></li>
                            </ul>
                            <input type="hidden" name="code" value="" />
                            <input type="hidden" name="redirect_route" value="product/category" />
                            <input type="hidden" name="redirect_query" value="&page=3&path=60" />
                            <input type="hidden" name="redirect_ssl" value="" />
                        </form>
                    </div>
                </div>
                <div id="menu" class="header-menu">
                    <ul class="header-menu-list">
                        <li><a href="http://malva/ru/tkanini-59/">Тканини</a></li>
                        <li class="dropdown">
                            <a href="http://malva/ru/domashniy-tekstil-60/">Домашній текстиль</a>
                            <ul class="header-menu-under">
                                <li><a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/">Постільна білизна</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/kovdri-65/">Ковдри</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/pledi-67/">Пледи</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/podushki-66/">Подушки</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/namatratsniki-68/">Наматрацники </a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/prostiradla-na-rezintsi-69/">Простирадла на резинці</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/halati-70/">Халати</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/rushniki-71/">Рушники</a></li>
                                <li><a href="http://malva/ru/domashniy-tekstil-60/mahrovi-kovriki-72/">Махрові коврики</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="http://malva/ru/gotelniy-tekstil-61/">Готельний текстиль</a>
                            <ul class="header-menu-under">
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/postilna-bilizna-73/">Постільна білизна</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/kovdri-74/">Ковдри</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/podushki-75/">Подушки</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/namatratsniki-76/">Наматрацники</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/halati-77/">Халати</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/rushniki-78/">Рушники</a></li>
                                <li><a href="http://malva/ru/gotelniy-tekstil-61/mahrovi-kovriki-79/">Махрові коврики</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="http://malva/ru/dityacha-produktsiya-62/">Дитяча продукція</a>
                            <ul class="header-menu-under">
                                <li><a href="http://malva/ru/dityacha-produktsiya-62/dityacha-postil-80/">Дитяча постіль</a></li>
                                <li><a href="http://malva/ru/dityacha-produktsiya-62/podushki-81/">Подушки</a></li>
                                <li><a href="http://malva/ru/dityacha-produktsiya-62/kovdri-82/">Ковдри</a></li>
                                <li><a href="http://malva/ru/dityacha-produktsiya-62/pledi-83/">Пледи</a></li>
                                <li><a href="http://malva/ru/dityacha-produktsiya-62/pelenki-84/">Пеленки</a></li>
                            </ul>
                        </li>
                        <li><a href="http://malva/ru/novinki-63/">Новинки</a></li>
                        <li class="dropdown-click">
                            <i class="icon-more"></i>
                            <ul class="header-menu-under">
                                <li><a href="http://malva/ru/specials/">Sale</a></li>
                                <li><a href="http://malva/ru/about_us/">О нас</a></li>                                                                                                                                                                                                                                                                                                                                        <li><a href="http://malva/ru/nblog/">Блог</a></li>
                                <li><a href="http://malva/ru/contact-us/">Контакты </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="modal fade modal_ac" id="modal-ac" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Returning Customer</h4>
            </div>
            <div class="modal-body">
                <div class="modal_login" id="modal-login">
                    <input type="text" name="email" value=""  id="input-email-l" class="form-control" placeholder="Email" />
                    <input type="password" name="password" value="" id="input-password-l" class="form-control" placeholder="Password" />
                    <div class="modal_help">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox-l" />
                            <label for="checkbox-l">text_open_pass</label>
                        </div>
                        <a href="http://malva/ru/forgot-password/">Forgotten Password</a>
                    </div>
                    <button type="button" class="btn loginaccount"  data-loading-text="Загрузка...">Login</button>
                    <p>text_login_social</p>
                    <div class="modal_social_log">
                        <a href="javascript:void(0);" onclick="social_auth.facebook(this)" data-loading-text="Loading">Facebook</a>
                        <a href="javascript:void(0);" onclick="social_auth.googleplus(this)" data-loading-text="Loading">Google+</a>
                        <a href="javascript:void(0);" onclick="social_auth.instagram(this)" data-loading-text="Loading">instagram</a>
                    </div>
                </div>
                <div class="modal_register" id="modal-register">
                    <input type="text" name="email" value="" id="input-email-r" class="form-control" placeholder="Email" />
                    <input type="password" name="password" value="" id="input-password-r" class="form-control" placeholder="Password" />
                    <input type="text" name="name" value="" id="input-name-r" class="form-control" placeholder="Name" />
                    <input type="text" name="surname" value="" id="input-surname-r" class="form-control" placeholder="entry_surname" />
                    <input type="text" name="telephone" value="" id="input-telephone-r" class="hide"/>
                    <div class="modal_help">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox-r" />
                            <label for="checkbox-r">text_open_pass</label>
                        </div>

                    </div>
                    <button type="button" class="btn btn-max-st2 createaccount" data-loading-text="Загрузка..." >button_register</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script   ><!--
    $(document).delegate('.quick_login', 'click', function(e) {
        $('#modal-ac').modal('show');
    });
    //--></script>
<script   ><!--
    $('#modal-register input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#quick-register .createaccount').trigger('click');
        }
    });
    $('#modal-register .createaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/register',
            type: 'post',
            data: $('#modal-register input[type=\'text\'], #modal-register input[type=\'password\'], #modal-register input[type=\'checkbox\']:checked'),
            dataType: 'json',
            beforeSend: function() {
                $('#modal-register .createaccount').button('loading');
                $('#modal-register .alert').remove();
            },
            complete: function() {
                $('#modal-register .createaccount').button('reset');
            },
            success: function(json) {
                $('#modal-register input').removeClass('has-error');

                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }


                if (json['error_surname']) {
                    $('#modal-register #input-surname-r').addClass('has-error');
                    $('#modal-register #input-surname-r').focus();
                }

                if (json['error_name']) {
                    $('#modal-register #input-name-r').addClass('has-error');
                    $('#modal-register #input-name-r').focus();
                }

                if (json['error_password']) {
                    $('#modal-register #input-password-r').addClass('has-error');
                    $('#modal-register #input-password-r').focus();
                }

                if (json['error_email']) {
                    $('#modal-register #input-email-r').addClass('has-error');
                    $('#modal-register #input-email-r').focus();
                }
                if (json['error']) {
                    $('#modal-register .modal_help .check_box').after('<div class="alert">' + json['error'] + '</div>');
                }

                //if (json['now_login']) {
                //	$('.quick-login').before('<li class="dropdown"><a href="//" title="//" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">//</span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="//">//</a></li><li><a href="//">//</a></li><li><a href="//">//</a></li><li><a href="//">//</a></li><li><a href="//">//</a></li></ul></li>');
                //
                //	$('.quick-login').remove();
                //}
                if (json['success']) {
                    console.log(json['heading_title']);
                    $('#modal-ac .modal-title').html(json['heading_title']);
                    success = json['text_message'];
                    success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
                    $('#modal-ac .modal-body').html(success);
                }
            }
        });
    });
    //--></script>
<script   ><!--
    $('#modal-login input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#modal-login .loginaccount').trigger('click');
        }
    });
    $('#modal-login .loginaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/login',
            type: 'post',
            data: $('#modal-login input[type=\'text\'], #modal-login input[type=\'password\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#modal-login .loginaccount').button('loading');
                $('#modal-login .alert').remove();
            },
            complete: function() {
                $('#modal-login .loginaccount').button('reset');
            },
            success: function(json) {
                $('#modal-login .form-group').removeClass('has-error');
                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }

                if (json['error']) {
                    $('#modal-login .modal_help a').after('<div class="alert">' + json['error'] + '</div>');
                    $('#modal-login #input-email').parent().addClass('has-error');
                    $('#modal-login #input-password').parent().addClass('has-error');
                    $('#modal-login #input-email').focus();
                }
                if(json['success']){
                    loacation();
                    $('#modal-login').modal('hide');
                }

            }
        });
    });
    //--></script>
<script   ><!--
    function loacation() {
        location.reload();
    }
    //--></script>
<div class="breadcrumb-block">
    <div class="container">
        <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="http://malva/ru/" href="http://malva/ru/">
                    <span itemprop="name"><i class="fa fa-home"></i>Главная</span>
                </a>
                <meta itemprop="position" content="0" />
            </li>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="http://malva/ru/domashniy-tekstil-60/">
                            <span itemprop="name">Домашній текстиль</span>
                        </span>
                <meta itemprop="position" content="1" />
            </li>
        </ul>
    </div>
</div>
<div class="catalog_preview">
    <div class="container">
        <div class="catalog_preview-container">
            <div class="catalog_preview-content">
                <h1 class="catalog_preview-title">Домашній текстиль</h1>
                <div class="catalog_preview-description"><p><span style="color: rgb(51, 51, 51); font-family: Calibri, sans-serif; font-size: 15px; background-color: rgb(252, 242, 244);">Компанія використовує самі передові технології і натуральні якісні матеріали, які не завдають шкоди навколишньому середовищу і здоров'ю людини.</span><br></p></div>
            </div>
            <div class="catalog_preview-img">
                <img src="http://malva/image/cache/catalog/categoru/pic-catalog-755x500.png" alt="Домашній текстиль" title="Домашній текстиль"/>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row"><aside id="column-left" class="col-sm-3 hidden-xs">
            <h2>Категории</h2>
            <div class="category_list">
                <ul>
                    <li class="">

                        <a href="http://malva/ru/tkanini-59/">Тканини</a>
                    </li>
                    <li class=" active">

                        <a href="http://malva/ru/domashniy-tekstil-60/">Домашній текстиль</a>
                        <i class="icon-down-arrow"></i>
                        <ul>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/">Постільна білизна <span>(23)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/kovdri-65/">Ковдри <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/pledi-67/">Пледи <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/podushki-66/">Подушки <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/namatratsniki-68/">Наматрацники  <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/prostiradla-na-rezintsi-69/">Простирадла на резинці <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/halati-70/">Халати <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/rushniki-71/">Рушники <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/domashniy-tekstil-60/mahrovi-kovriki-72/">Махрові коврики <span>(0)</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="">

                        <a href="http://malva/ru/gotelniy-tekstil-61/">Готельний текстиль</a>
                        <i class="icon-down-arrow"></i>
                        <ul>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/postilna-bilizna-73/">Постільна білизна <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/kovdri-74/">Ковдри <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/podushki-75/">Подушки <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/namatratsniki-76/">Наматрацники <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/halati-77/">Халати <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/rushniki-78/">Рушники <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/gotelniy-tekstil-61/mahrovi-kovriki-79/">Махрові коврики <span>(0)</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="">

                        <a href="http://malva/ru/dityacha-produktsiya-62/">Дитяча продукція</a>
                        <i class="icon-down-arrow"></i>
                        <ul>
                            <li>
                                <a href="http://malva/ru/dityacha-produktsiya-62/dityacha-postil-80/">Дитяча постіль <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/dityacha-produktsiya-62/podushki-81/">Подушки <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/dityacha-produktsiya-62/kovdri-82/">Ковдри <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/dityacha-produktsiya-62/pledi-83/">Пледи <span>(0)</span></a>
                            </li>
                            <li>
                                <a href="http://malva/ru/dityacha-produktsiya-62/pelenki-84/">Пеленки <span>(0)</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="">

                        <a href="http://malva/ru/novinki-63/">Новинки</a>
                    </li>
                    <li><a href="http://malva/ru/specials/">text_special</a></li>
                </ul>
            </div>
            <script   >
                MegaFilter.prototype.beforeRequest = function() {
                    var self = this;
                };

                MegaFilter.prototype.beforeRender = function( htmlResponse, htmlContent, json ) {
                    var self = this;
                };

                MegaFilter.prototype.afterRender = function( htmlResponse, htmlContent, json ) {
                    var self = this;
                };
            </script>



            <div class="box mfilter-box mfilter-column_left" id="mfilter-box-1">
                <h2 class="box-heading">Фильтр</h2>
                <div class="category_filter mfilter-content">
                    <ul>
                        <li
                            data-type="checkbox"
                            data-base-type="attribute"
                            data-id="44"
                            data-seo-name="44-tip-tkanini"
                            data-inline-horizontal="0"
                            data-display-live-filter="0"
                            data-display-list-of-items=""
                            class="mfilter-filter-item mfilter-checkbox mfilter-attribute mfilter-attributes"
                        >

                            <div class="mfilter-heading">
                                <div class="mfilter-heading-content">
                                    <div class="mfilter-heading-text"><span>Тип тканини</span></div>
                                    <i class="icon-down-arrow"></i>
                                </div>
                            </div>

                            <div class="mfilter-content-opts">
                                <div class="mfilter-opts-container">
                                    <div class="mfilter-content-wrapper">
                                        <div class="mfilter-options">
                                            <div class="mfilter-options-container">
                                                <div class="mfilter-tb">
                                                    <div class="mfilter-option mfilter-tb-as-tr">
                                                        <input
                                                            id="mfilter-opts-attribs-1-44-69bd7b1e23ee41ec2dbb0d8e6d54bf86"
                                                            name="44-tip-tkanini"
                                                            type="checkbox"
                                                            value="Сатин"
                                                            class="checkbox"/>
                                                        <label class="mfilter-tb-as-td" for="mfilter-opts-attribs-1-44-69bd7b1e23ee41ec2dbb0d8e6d54bf86">
                                                            Сатин<span> (<span class="mfilter-counter">0</span>)</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li
                            data-type="checkbox"
                            data-base-type="option"
                            data-id="11"
                            data-seo-name="11o-rozm-r"
                            data-inline-horizontal="0"
                            data-display-live-filter="0"
                            data-display-list-of-items=""
                            class="mfilter-filter-item mfilter-checkbox mfilter-option mfilter-options"
                        >

                            <div class="mfilter-heading">
                                <div class="mfilter-heading-content">
                                    <div class="mfilter-heading-text"><span>Розмір</span></div>
                                    <i class="icon-down-arrow"></i>
                                </div>
                            </div>

                            <div class="mfilter-content-opts">
                                <div class="mfilter-opts-container">
                                    <div class="mfilter-content-wrapper">
                                        <div class="mfilter-options">
                                            <div class="mfilter-options-container">
                                                <div class="mfilter-tb">
                                                    <div class="mfilter-option mfilter-tb-as-tr">
                                                        <input
                                                            id="mfilter-opts-attribs-1-11-47"
                                                            name="11o-rozm-r"
                                                            type="checkbox"
                                                            value="47"
                                                            class="checkbox"/>
                                                        <label class="mfilter-tb-as-td" for="mfilter-opts-attribs-1-11-47">
                                                            полуторний<span> (<span class="mfilter-counter">0</span>)</span>
                                                        </label>
                                                    </div>
                                                    <div class="mfilter-option mfilter-tb-as-tr">
                                                        <input
                                                            id="mfilter-opts-attribs-1-11-48"
                                                            name="11o-rozm-r"
                                                            type="checkbox"
                                                            value="48"
                                                            class="checkbox"/>
                                                        <label class="mfilter-tb-as-td" for="mfilter-opts-attribs-1-11-48">
                                                            двоспальний<span> (<span class="mfilter-counter">0</span>)</span>
                                                        </label>
                                                    </div>
                                                    <div class="mfilter-option mfilter-tb-as-tr">
                                                        <input
                                                            id="mfilter-opts-attribs-1-11-49"
                                                            name="11o-rozm-r"
                                                            type="checkbox"
                                                            value="49"
                                                            class="checkbox"/>
                                                        <label class="mfilter-tb-as-td" for="mfilter-opts-attribs-1-11-49">
                                                            евро<span> (<span class="mfilter-counter">0</span>)</span>
                                                        </label>
                                                    </div>
                                                    <div class="mfilter-option mfilter-tb-as-tr">
                                                        <input
                                                            id="mfilter-opts-attribs-1-11-50"
                                                            name="11o-rozm-r"
                                                            type="checkbox"
                                                            value="50"
                                                            class="checkbox"/>
                                                        <label class="mfilter-tb-as-td" for="mfilter-opts-attribs-1-11-50">
                                                            сім’я<span> (<span class="mfilter-counter">0</span>)</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li
                            data-type="price"
                            data-base-type="price"
                            data-id="price"
                            data-seo-name="price"
                            data-inline-horizontal="0"
                            data-display-live-filter="0"
                            data-display-list-of-items=""
                            class="mfilter-filter-item mfilter-price mfilter-price"
                        >

                            <div class="mfilter-heading">
                                <div class="mfilter-heading-content">
                                    <div class="mfilter-heading-text"><span>Цена</span></div>
                                    <i class="icon-down-arrow"></i>
                                </div>
                            </div>

                            <div class="mfilter-content-opts">
                                <div class="mfilter-opts-container">
                                    <div class="mfilter-content-wrapper">
                                        <div class="mfilter-options">
                                            <div class="mfilter-option mfilter-price">
                                                <div class="mfilter-price-inputs">

                                                    <input
                                                        id="mfilter-opts-price-min"
                                                        type="text"
                                                        class="form-control"
                                                        value=""
                                                    />

                                                    <input
                                                        id="mfilter-opts-price-max"
                                                        type="text"
                                                        class="form-control"
                                                        value=""
                                                    />

                                                </div>
                                                <div class="mfilter-price-slider">
                                                    <div id="mfilter-price-slider"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="mfilter-button mfilter-button-bottom"><a href="#" class="mfilter-button-reset btn btn-max-st1">сбросить фильтры</a></div>	</div>
            </div>


            <script   >
                MegaFilterLang.text_display = '<b>Notice</b>: Undefined variable: text_display in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>373</b>';
                MegaFilterLang.text_list	= '<b>Notice</b>: Undefined variable: text_list in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>374</b>';
                MegaFilterLang.text_grid	= '<b>Notice</b>: Undefined variable: text_grid in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>375</b>';
                MegaFilterLang.text_select	= ' --- Выберите --- ';

                jQuery().ready(function(){
                    jQuery('#mfilter-box-1').each(function(){
                        var _t = jQuery(this).addClass('init'),
                            _p = { };

                        for( var i = 0; i < MegaFilterINSTANCES.length; i++ ) {
                            if( _t.attr('id') == MegaFilterINSTANCES[i]._box.attr('id') ) {
                                return;
                            }
                        }

                        _p['path'] = '60';

                        MegaFilterINSTANCES.push((new MegaFilter()).init( _t, {
                            'idx'					: '1',
                            'route'					: 'cHJvZHVjdC9jYXRlZ29yeQ==',
                            'routeProduct'			: 'cHJvZHVjdC9wcm9kdWN0',
                            'routeHome'				: 'Y29tbW9uL2hvbWU=',
                            'routeCategory'			: 'cHJvZHVjdC9jYXRlZ29yeQ==',
                            'contentSelector'		: '#mfilter-content-container',
                            'refreshResults'		: 'immediately',
                            'refreshDelay'			: 1000,
                            'autoScroll'			: false,
                            'ajaxInfoUrl'			: 'http://malva/ru/index.php?route=module/mega_filter/ajaxinfo',
                            'ajaxResultsUrl'		: 'http://malva/ru/index.php?route=module/mega_filter/results',
                            'ajaxCategoryUrl'		: 'http://malva/ru/index.php?route=module/mega_filter/categories',
                            'priceMin'				: 120,
                            'priceMax'				: 4400,
                            'mijoshop'				: false,
                            'joo_cart'				: false,
                            'showNumberOfProducts'	: true,
                            'calculateNumberOfProducts' : true,
                            'addPixelsFromTop'		: 0,
                            'displayListOfItems'	: {
                                'type'				: 'scroll',
                                'limit_of_items'	: 4,
                                'maxHeight'			: 155,
                                'textMore'			: '<b>Notice</b>: Undefined variable: text_show_more in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>417</b>',
                                'textLess'			: '<b>Notice</b>: Undefined variable: text_show_less in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>418</b>'
                            },
                            'smp'					: {
                                'isInstalled'			: false,
                                'disableConvertUrls'	: false				},
                            'params'					: _p,
                            'inStockDefaultSelected'	: false,
                            'inStockStatus'				: '7',
                            'showLoaderOverResults'		: true,
                            'showLoaderOverFilter'		: false,
                            'hideInactiveValues'		: false,
                            'manualInit'				: false,
                            'homePageAJAX'				: false,
                            'homePageContentSelector'	: '#content',
                            'ajaxPagination'			: false,
                            'text'						: {
                                'loading'		: 'Загрузка...',
                                'go_to_top'		: '<b>Notice</b>: Undefined variable: text_go_to_top in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>436</b>',
                                'init_filter'	: '<b>Notice</b>: Undefined variable: text_init_filter in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>437</b>',
                                'initializing'	: '<b>Notice</b>: Undefined variable: text_initializing in <b>/opt/www/malva/catalog/view/theme/default/template/module/mega_filter.tpl</b> on line <b>438</b>'
                            },
                            'direction'				: 'ltr',
                            'seo' : {
                                'enabled'	: false,
                                'alias'		: ''
                            }
                        }));
                    });
                });
            </script>
        </aside>
        <div id="content" class="col-sm-9"><div id="mfilter-content-container">

                <div class="catalog_sort">
                    <ul>
                        <li>Сортировать:</li>
                        <li class="active"><a href="http://malva/ru/domashniy-tekstil-60/?sort=p.sort_order&amp;order=ASC">по умолчанию</a></li>
                        <li><a href="http://malva/ru/domashniy-tekstil-60/?sort=p.date_added&amp;order=DESC">сначала новинки</a></li>
                        <li><a href="http://malva/ru/domashniy-tekstil-60/?sort=p.sort_special&amp;order=ASC">сначала акционные</a></li>
                        <li><a href="http://malva/ru/domashniy-tekstil-60/?sort=p.price&amp;order=ASC">цена по росту</a></li>
                        <li><a href="http://malva/ru/domashniy-tekstil-60/?sort=p.price&amp;order=DESC">цена по убыванию</a></li>
                    </ul>
                </div>
                <div class="products">
                    <div class="row">
                        <div class="product-layout">
                            <div class="product-thumb transition ">
                                <div class="image">
                                    <a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-56/">
                                        <img src="http://malva/image/cache/catalog/product/pic-3-263x263.png" alt="Cатиновий комплект" class="img-min"/>
                                        <img src="http://malva/image/cache/catalog/product/pic-6-555x263.png" alt="Cатиновий комплект" class="img-max"/>
                                    </a>
                                    <div class="image-label">
                                        <span>New</span>            </div>
                                    <div class="button-group">
                                        <button class="btn-wishlist" type="button"
                                                onclick="wishlist.add('56');"><i class="icon-heart"></i>
                                        </button>
                                        <button class="btn-cart" type="button" onclick="cart.add('56');"><i
                                                class="icon-shopping-bag"></i> <span
                                                class="">В корзину</span></button>
                                    </div>
                                </div>

                                <div class="caption">
                                    <div class="caption-name"><a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-56/">Cатиновий комплект</a></div>
                                    <p class="price">
                                        <span class="price-new">2 200 uah</span>
                                    </p>
                                </div>
                            </div>
                        </div>                            <div class="product-layout">
                            <div class="product-thumb transition ">
                                <div class="image">
                                    <a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-70/">
                                        <img src="http://malva/image/cache/catalog/product/pic-7-263x263.png" alt="Cатиновий комплект" class="img-min"/>
                                        <img src="http://malva/image/cache/catalog/product/pic-1-555x263.png" alt="Cатиновий комплект" class="img-max"/>
                                    </a>
                                    <div class="image-label">
                                        <span>New</span>            </div>
                                    <div class="button-group">
                                        <button class="btn-wishlist" type="button"
                                                onclick="wishlist.add('70');"><i class="icon-heart"></i>
                                        </button>
                                        <button class="btn-cart" type="button" onclick="cart.add('70');"><i
                                                class="icon-shopping-bag"></i> <span
                                                class="">В корзину</span></button>
                                    </div>
                                </div>

                                <div class="caption">
                                    <div class="caption-name"><a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-70/">Cатиновий комплект</a></div>
                                    <p class="price">
                                        <span class="price-new">1 111 uah</span>
                                    </p>
                                </div>
                            </div>
                        </div>                            <div class="product-layout">
                            <div class="product-thumb transition ">
                                <div class="image">
                                    <a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-64/">
                                        <img src="http://malva/image/cache/catalog/product/pic-1-1-263x263.png" alt="Cатиновий комплект" class="img-min"/>
                                        <img src="http://malva/image/cache/catalog/product/pic-6-555x263.png" alt="Cатиновий комплект" class="img-max"/>
                                    </a>
                                    <div class="image-label">
                                        <span>-7%</span><span>New</span>            </div>
                                    <div class="button-group">
                                        <button class="btn-wishlist" type="button"
                                                onclick="wishlist.add('64');"><i class="icon-heart"></i>
                                        </button>
                                        <button class="btn-cart" type="button" onclick="cart.add('64');"><i
                                                class="icon-shopping-bag"></i> <span
                                                class="">В корзину</span></button>
                                    </div>
                                </div>

                                <div class="caption">
                                    <div class="caption-name"><a href="http://malva/ru/domashniy-tekstil-60/postilna-bilizna-64/catinoviy-komplekt-64/">Cатиновий комплект</a></div>
                                    <p class="price">
                                        <span class="price-new">1 300 uah</span> <span
                                            class="price-old">1 400 uah</span>
                                    </p>
                                </div>
                            </div>
                        </div>                      </div>
                </div>
                <div class="prod_trigger">
                    <div class="prod_trigger-info">
                        <p>Отображается <span class="p_ins">23</span> из <span class="p_max">23</span> товарів</p>
                        <div class="prod_trigger-line">
                            <span></span>
                        </div>
                    </div>
                    <div class="prod_trigger-a">
                        <a href="/" class="btn btn-max-default ">загрузить еще</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 text-left"><ul class="pagination"><li><a href="http://malva/ru/domashniy-tekstil-60/">|&lt;</a></li><li><a href="http://malva/ru/domashniy-tekstil-60/page-2">&lt;</a></li><li><a href="http://malva/ru/domashniy-tekstil-60/">1</a></li><li><a href="http://malva/ru/domashniy-tekstil-60/page-2">2</a></li><li class="active"><span>3</span></li></ul></div>
                </div>
            </div></div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h5>Мальва</h5>
                <ul class="list-unstyled">
                    <li><a href="http://malva/ru/about_us/">О нас</a></li>                                                                                                                                                                                            <li><a href="http://malva/ru/nblog/">Блог</a></li>
                    <li><a href="http://malva/ru/contact-us/">Контакты</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>Продукция</h5>
                <ul class="list-unstyled">
                    <li><a href="http://malva/ru/tkanini-59/">Тканини</a></li>
                    <li><a href="http://malva/ru/domashniy-tekstil-60/">Домашній текстиль</a></li>
                    <li><a href="http://malva/ru/gotelniy-tekstil-61/">Готельний текстиль</a></li>
                    <li><a href="http://malva/ru/dityacha-produktsiya-62/">Дитяча продукція</a></li>
                    <li><a href="http://malva/ru/novinki-63/">Новинки</a></li>
                    <li><a href="http://malva/ru/specials/">Sale</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>клиентам</h5>
                <ul class="list-unstyled">

                    <li><a href="http://malva/ru/yak-obrati-rozmir-postilnoi-bilizniiiii-copy-18/">Доставка і оплата</a></li>
                    <li><a href="http://malva/ru/yak-obrati-rozmir-postilnoi-bilizniiiii-copy-19/">Як зробити замовлення?</a></li>
                    <li><a href="http://malva/ru/yak-obrati-rozmir-postilnoi-bilizniiiii-copy-copy-20/">Як обрати розмір постільної білизни?</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>Контакты</h5>
                <div class="footer-address">Хмельницький, вул. Назва вулиці 00/0</div>              <div class="footer-tel"><a href="tel:+380680000001">+38 (068) 000-00-01</a></div>              <div class="footer-mail"><a href="mail:malvakm@gmail.com.ua">malvakm@gmail.com.ua</a></div>          </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="copyright-content">
            <div class="copyright-content-text"> &copy; Copyright. Malva 2018. Все права защищены</div>
            <a href="https://web-systems.solutions/" rel="nofollow noopener" rel="nofollow noopener" class="copyright-content-img" target="_blank">
            </a>
        </div>
    </div>
</div>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->


<div class="body_call-back">
    <button  type="button" data-toggle="modal" data-target="#request_call-modal"><i class="icon-phone-call-body"></i> <span>Обратный звонок</span></button>
</div>
<div class="modal fade request_call" id="request_call-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon-close"></i></button>
            <div class="modal-header">

                <h3 class="modal-title">Заказать звонок</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal request_call-form" id="request_call-form">
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="name" id="input-name" placeholder="Ваше имя" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="phone" id="input-phone" placeholder="Телефон" class="form-control"/>
                        </div>
                    </div>
                    <input name="goal" value="callback_request" type="hidden">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="request_call-btn" data-loading-text="Обработка" class="btn">відправити</button>
            </div>
        </div>
    </div>
</div>
<script   >
    $('#request_call-btn').on('click', function () {
        $.ajax({
            url: 'index.php?route=sendmess/send_message/send',
            type: 'post',
            dataType: 'json',
            data:  $("#request_call-form").serialize(),
            beforeSend: function () {
                if ($("textarea").is("#g-recaptcha-response")) {
                    grecaptcha.reset();
                }
                $('#request_call-btn').button('loading');
            },
            complete: function () {
                $('#request_call-btn').button('reset');
            },
            success: function (json) {
                if (json['error']) {
                    $('.error').remove();
                    if (json['error_name']){
                        $('#request_call-form input[name="name"]').after('<label class="error">' + json['error_name'] + '</label>');
                        //$('#request_call-form input[name="name"]').parent().parent('.form-group').addClass('has-error');
                    }
                    if (json['error_phone']){
                        $('#request_call-form input[name="phone"]').after('<label class="error">' + json['error_phone'] + '</label>');
                        //$('#request_call-form input[name="phone"]').parent().parent('.form-group').addClass('has-error');
                    }
                }
                if (json['success']) {
                    $('#request_call-form .error').remove();
                    $('input[name=\'name\']').val('');
                    $('input[name=\'phone\']').val('');

                    $('#request_call-modal').modal('hide');
                    $('.header .container .row').before('<div class="user_message_wrapp alert" style="opacity: 0"> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('.user_message_wrapp').animate({
                        opacity: 1
                    }, 300);
                    setTimeout(function () {
                        $('.alert').detach();
                    }, 5100);
                }
            }
        });
    });
</script>

</body></html><script>

</script>