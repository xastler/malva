
// document.oncontextmenu = nocontext;
// function nocontext(evt) {
//     var evt = (evt) ? evt : window.event;
//     console.log(evt);
//
//     var target = evt.target || evt.srcElement;
//     console.log(target);
//
//     if(target.tagName.toLowerCase()=='img'){
//         return false;
//     }
// }
$(document).ready(function () {
    function click(e) {
        if (document.all) {    // IE
            if (event.button == 2) {    // Чтобы отключить левую кнопку поставьте цифру 1
                return false;}
        }
        if (document.layers) { // NC
            if (e.which == 3) {
                return false;}
        }
    }
    if (document.layers)
    {document.captureEvents(Event.MOUSEDOWN);}
    document.onmousedown=click;
    document.oncontextmenu=function(e){return false};


    if($('*').is('.wrapper_special') && !$('*').is('.baner_special')) {
        $('.wrapper_special').addClass('one_block');
    }
    else if(!$('*').is('.wrapper_special') && $('*').is('.baner_special')) {
        $('.baner_special').addClass('one_block');
    }

    $("#menu .dropdown").hover(function () {
        $(this).children('ul').toggle();
    });
    $("#menu .dropdown-click").hover(function () {
        $(this).children('ul').toggle();
    });

    // $("#menu .dropdown-click").click(function () {
    //     $(this).children('ul').slideToggle();
    //     $(document).mouseup(function (e){ // событие клика по веб-документу
    //         var div = $('#menu .dropdown-click .header-menu-under'); // тут указываем ID элемента
    //         if (!div.is(e.target) // если клик был не по нашему блоку
    //             && div.has(e.target).length === 0) { // и не по его дочерним элементам
    //             div.slideUp(); // скрываем его
    //         }
    //     });
    // });
    if($('.home_about-content').height()<=140){
        $(".home_about-open").hide();
        $(".home_about-content").addClass('open');

    }else{
        $('.home_about-content').css('max-height','134px');
        $('.seo_text').css('margin-bottom','0');
    }
    $(".home_about-open").click(function () {
        $('.home_about-content').addClass('mh_max-content');
        $(".home_about-content").addClass('open');
        $(this).css('opacity','0');
        $(".home_about-open").slideUp();
    });


    $(".category_list li i").click(function () {
        $(this).next('ul').slideToggle();
        $(this).parent('li').toggleClass('active');
    });

    $(".catalog_sort-btn").click(function () {
        $(this).next('ul').slideToggle();
        $(this).toggleClass('active');
    });

    $(".megafilter-btn").click(function () {
        $('.mfilter-box').addClass('active');
        $('body').addClass('overflow_popups');
    });
    $(".mfilter-box .box-heading i").click(function () {
        $('.mfilter-box').removeClass('active');
        $('body').removeClass('overflow_popups');
    });

    $('.tab-btn-mob').click(function () {
        $(this).next().slideToggle();
        $(this).toggleClass('active');
    });

    var x = Number($(".prod_trigger p .p_ins").html());
    var y = Number($(".prod_trigger p .p_max").html());
    var x_line ;
    if((x+x)>y){
        x_line = x*(100/y);
        $(".prod_trigger .prod_trigger-line span").css('width',x_line + '%');
    }
    if(x_line==100){
        $('.prod_trigger-a a').hide();
    }

    $('body').on('click', '.prod_trigger-a a', function (e) {
        e.preventDefault();
        var tar = $(this);
        $('.pagination li').each(function (i, el) {
            if ($(el).hasClass('active')) {
                $(el).removeClass('active');
                $(el).next().addClass('active');
                // $("<div class='load-" + i + "'></div>").appendTo('.bnews-list');
                $("<div class='load'></div>").appendTo('body');
                $('.load').hide();
                // console.log($(el).next().find('a').attr('href'));
                // console.log(encodeURIComponent($(el).next().find('a').attr('href')));
                // return false;
                $('.load').load(encodeURI($(el).next().find('a').attr('href')) + ' .products .row ', function () {
                    $(".products>.row").append($('.load>.row'));
                    var x = Number($(".prod_trigger p .p_ins").html());
                    var y = Number($(".prod_trigger p .p_max").html());
                    var x_line ;
                    if((x+10)>y){
                        $(".prod_trigger p .p_ins").html(y);
                        $(".prod_trigger .prod_trigger-line span").css('width','100%');
                    }else{
                        x=x+10;
                        x_line = x*(100/y);
                        $(".prod_trigger p .p_ins").html(x);
                        $(".prod_trigger .prod_trigger-line span").css('width',x_line + '%');
                    }
                    $('.load').detach();
                });
                // $('#mfilter-content-container').parent('div').find('.pagination').append('<div id="ajaxblock" style="width:' + w + 'px;height:30px;margin-top:20px;text-align:center;border:none !important;"><img src="../image/preload.gif" /></div>');
                if (i + 4 == $('.pagination li').length) {
                    tar.hide();
                }
                return false;
            }
        });
    });
    $('#checkbox-l').on('change', function () {
        var $el = $(this);
        if ($el.is(':checked')) {
            $('#input-password-l').attr('type', 'text');
        } else {
            $('#input-password-l').attr('type', 'password');
        }
    });
    $('#pag-checkbox-l').on('change', function () {
        var $el = $(this);
        if ($el.is(':checked')) {
            $('#input-password').attr('type', 'text');
        } else {
            $('#input-password').attr('type', 'password');
        }
    });
    $('#checkbox-r').on('change', function () {
        var $el = $(this);
        if ($el.is(':checked')) {
            $('#input-password-r').attr('type', 'text');
        } else {
            $('#input-password-r').attr('type', 'password');
        }
    });

    (function(){
        $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('+38 (999) 999-99-99');

        var j = jQuery; //Just a variable for using jQuery without conflicts
        var addInput = '#input-quantity'; //This is the id of the input you are changing
        var n = 1; //n is equal to 1

        //Set default value to n (n = 1)
        j(addInput).val(n);

        //On click add 1 to n
        j('.plus').on('click', function () {
            if (n <= 99) {
                j(addInput).val(++n);
            } else {
                //Otherwise do nothing
            }

        })

        j('.minus').on('click', function () {
            //If n is bigger or equal to 1 subtract 1 from n
            if (n >= 2) {
                j(addInput).val(--n);
            } else {
                //Otherwise do nothing
            }
        });

        $('select').styler();
    })();

    var offset = $('header').height();

    if ($(window).scrollTop() > offset) {
        $('header').addClass('fixed');
        $('body').addClass('fixed_menu');
    }
    else {
        $('header').removeClass('fixed');
        $('body').removeClass('fixed_menu');
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset) {
            $('header').addClass('fixed');
            $('body').addClass('fixed_menu');
        }
        else {
            $('header').removeClass('fixed');
            $('body').removeClass('fixed_menu');
        }
    });
    
    $('.search').click(function () {
        $('.search').addClass('active');
    });
    $('.product_preview-img_main').click(function () {
        $('.product_preview-img .thumbnails li:first-child a').click();
    });
    $(document).on('click', function (e) {
        if (!$(e.target).closest("#search").length) {
            $('.search').removeClass('active');
        }
        e.stopPropagation();
    });
    if($(window).width()<=768){
        $('.footer h5').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass('active');
        });
    }

    if ($(window).width() < 768) {
        $('.left_bar .left_bar-navigation').append($('#language'));
    }


    $('.left_bar-content span').click(function () {
        $(this).next().slideToggle();
        $(this).toggleClass('active');
    });

    $('.right_bar-content i').click(function () {
        $(this).next().slideToggle();
        $(this).parent().toggleClass('active');
    });

    $('.btn-right_bar').click(function () {
        $('.right_bar').addClass('active');
        $('body').addClass('overflow_popups');
    });
    $('.btn-left_bar').click(function () {
        $('.left_bar').addClass('active');
        $('body').addClass('overflow_popups');
    });
    $('.right_bar-close').click(function () {
        $('.right_bar').removeClass('active');
        $('body').removeClass('overflow_popups');
    });
    $('.left_bar-close').click(function () {
        $('.left_bar').removeClass('active');
        $('body').removeClass('overflow_popups');
    });

    if($(window).width()<=1024){
        $('.simplecheckout-step').after($('.buttons#buttons'));
    }

    if($(window).width()>=992 && $(window).width()<=1920){
        var x = $(window).width();
        var x2 = x/(1920/100);
        var y = x2*(755/100);
        var y2 = x2*(500/100);
        var block_h = x2*(454/100);
        var block_padding = x2*(92/100);
        var block_margin= x2*(53/100);
        var block_margin_m = x2 * (180 / 100);
        $('.js_preview').css('height',block_h);
        $('.js_preview').css('margin-bottom', block_margin_m);
        $('.js_preview-content').css('padding-top',block_padding);
        $('.js_preview-title').css('margin-bottom',block_margin);
        $('.js_preview-img img').css('width',y);
        $('.js_preview-img .block_img').css('width',y);
        $('.js_preview-img .block_img').css('height',y2);
    }
    var windowWidth = $(window).width();
    $(window).resize(function () {
        if ($(window).width() != windowWidth) {
            if($(window).width()>=992 && $(window).width()<=1920) {
                var x = $(window).width();
                var x2 = x / (1920 / 100);
                var y = x2 * (755 / 100);
                var y2 = x2*(500/100);
                var block_h = x2 * (454 / 100);
                var block_padding = x2 * (92 / 100);
                var block_margin = x2 * (53 / 100);
                var block_margin_m = x2 * (180 / 100);
                $('.js_preview').css('height', block_h);
                $('.js_preview').css('margin-bottom', block_margin_m);
                $('.js_preview-content').css('padding-top', block_padding);
                $('.js_preview-title').css('margin-bottom', block_margin);
                $('.js_preview-img img').css('width', y);
                $('.js_preview-img .block_img').css('width', y);
                $('.js_preview-img .block_img').css('height', y2);
            }else{
                    window.location.reload();
            }
        }
    });

    $('.btn-wishlist').click(function () {
        $(this).addClass('active');
        $(this).attr('disable');
    });


});
