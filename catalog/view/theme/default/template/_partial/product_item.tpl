<?php /* ?>
<div class="product-layout product-list col-sm-4">
    <div class="product-thumb">
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div>
            <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['rating']) { ?>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                    <p class="price">
                        <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                        <?php } else { ?>
                            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                        <?php } ?>
                        <?php if ($product['tax']) { ?>
                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                        <?php } ?>
                    </p>
                <?php } ?>
            </div>
            <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
        </div>
    </div>
</div>
<?php */ ?>
<div class="product-layout">
    <div class="product-thumb transition ">
        <div class="image">
            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-min"/>
                <img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-min_hover"/>
                <img src="<?php echo $product['thumb_max']; ?>" alt="<?php echo $product['name']; ?>" class="img-max"/>
                <img src="<?php echo $product['thumb_max_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-max_hover"/>
            </a>
            <div class="image-label">
                <?php foreach ($product['labels'] as $label ) { ?><span><?php echo $label['text']; ?></span><?php } ?>
            </div>
            <div class="button-group">
                <button class="btn-wishlist <?php echo ($product['wishlist_status'])?'active':''; ?>" type="button"
                        onclick="wishlist.add('<?php echo $product['product_id']; ?>');return false;"><i class="icon-heart"></i>
                </button>
                <button class="btn-cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                            class="icon-shopping-bag"></i> <span
                            class=""><?php echo $button_cart; ?></span></button>
            </div>
        </div>

        <div class="caption">
            <div class="caption-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <span class="price-new"><?php echo $product['price']; ?></span>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span
                                class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
    </div>
</div>