<div class="product-layout">
    <div class="product-thumb transition ">
        <div class="image">
            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-min"/>
                <img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-min_hover"/>
                <img src="<?php echo $product['thumb_max']; ?>" alt="<?php echo $product['name']; ?>" class="img-max"/>
                <img src="<?php echo $product['thumb_max_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-max_hover"/>
            </a>
            <div class="image-label">
                <?php foreach ($product['labels'] as $label ) { ?><span><?php echo $label['text']; ?></span><?php } ?>
            </div>
            <div class="button-group">
                <button class="btn-wishlist <?php echo ($product['wishlist_status'])?'active':''; ?>" type="button"
                        onclick="wishlist.add('<?php echo $product['product_id']; ?>');return false;"><i class="icon-heart"></i>
                </button>
                <button class="btn-cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                            class="icon-shopping-bag"></i> <span
                            class=""><?php echo $button_cart; ?></span></button>
            </div>
        </div>

        <div class="caption">
            <div class="caption-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <span class="price-new"><?php echo $product['price']; ?></span>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span
                                class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
    </div>
</div>