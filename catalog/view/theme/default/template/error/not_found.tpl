<?php echo $header; ?>
    <div class="breadcrumb-block" hidden>
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<div class="page_404">
    <div class="container">
          <div class="item-block">
              <div class="item-block-bg"></div>
              <div class="item-block-content">
                 <h1><?php echo $heading_title_txt; ?></h1>
                  <p class="item-block-txt2"><?php echo $text_error; ?></p>
                  <a href="<?php echo $continue; ?>" class="btn" tabindex="0"><?php echo $button_home; ?></a>
              </div>

          </div>
          <?php echo $content_bottom; ?>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>