<?php /*if ($is_author) { ?>
    <?php if ($author_image || $author_desc) { ?>
        <div class="category-info">
            <?php if ($author_image) { ?>
                <div class="image"><img src="<?php echo $author_image; ?>" alt="<?php echo $author; ?>"/></div>
            <?php } ?>
            <?php if ($author_desc) { ?>
                <?php echo $author_desc; ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } */?>
<?php if ($is_category) { ?>
    <?php /*if ($thumb || $description) { ?>
        <div class="category-info">
            <?php if ($thumb) { ?>
                <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"/></div>
            <?php } ?>
            <?php if ($description) { ?>
                <?php echo $description; ?>
            <?php } ?>
        </div>
    <?php }*/ ?>
    <?php if ($ncategories) { ?>
        <h2><?php echo $text_refine; ?></h2>
        <div class="category-list" style="border-bottom: 2px solid #eee;">
            <?php if (count($ncategories) <= 5) { ?>
                <ul>
                    <?php foreach ($ncategories as $ncategory) { ?>
                        <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php for ($i = 0; $i < count($ncategories);) { ?>
                    <ul>
                        <?php $j = $i + ceil(count($ncategories) / 4); ?>
                        <?php for (; $i < $j; $i++) { ?>
                            <?php if (isset($ncategories[$i])) { ?>
                                <li>
                                    <a href="<?php echo $ncategories[$i]['href']; ?>"><?php echo $ncategories[$i]['name']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>
<?php if ($article) { ?>
    <?php $i = 1;
    foreach ($article as $articles) { ?>
        <?php if ($i == 1) { ?>

            <div class="artblock_first js_preview <?php if ($display_style) { ?> artblock-2<?php } ?>">
                <div class="container">
                    <div class="layout">
                        <div class="layout-content">
                            <h1 class="layout-title">Блог</h1>

                            <div class="layout-meta">
                                <?php if ($articles['date_added']) { ?>
                                    <?php echo $articles['date_added']; ?>
                                <?php } ?>
                            </div>
                            <?php if ($articles['name']) { ?>
                                <div class="layout-name"><?php echo $articles['name']; ?></div>
                            <?php } ?>
                            <?php if ($articles['description']) { ?>
                                <div class="layout-description"><?php echo $articles['description']; ?></div>
                            <?php } ?>
                            <?php if ($articles['button']) { ?>
                                <div class="layout-btn"><a
                                            href="<?php echo $articles['href']; ?>"><?php echo $button_more; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="layout-img js_preview-img">
                            <?php if ($articles['thumb_x']) { ?>
                                <img class="article-image"   src="<?php echo $articles['thumb_x']; ?>"
                                     title="<?php echo $articles['name']; ?>"
                                     alt="<?php echo $articles['name']; ?>, <?php echo $text_photo; ?>" />
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php $i++;
    } ?>
    <div class="container">
        <div class="bnews-list<?php if ($display_style) { ?> bnews-list-2<?php } ?>">
            <?php foreach ($article as $articles) { ?>
                <div class="box-artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">
                    <div class="artblock">
                        <?php if ($articles['thumb']) { ?>
                            <a href="<?php echo $articles['href']; ?>"><img class="article-image"
                                                                            src="<?php echo $articles['thumb']; ?>"
                                                                            title="<?php echo $articles['name']; ?>"
                                                                            alt="<?php echo $articles['name']; ?>, <?php echo $text_photo; ?>" /></a>
                        <?php } ?>
                        <div class="artblock-content">

                            <div class="article-meta">
                                <?php if ($articles['date_added']) { ?>
                                    <?php if ($articles['author']) { ?><?php echo $text_posted_on; ?><?php } else { ?><?php echo $text_posted_pon; ?><?php } ?><?php echo $articles['date_added']; ?> |
                                <?php } ?>
                            </div>
                            <?php if ($articles['name']) { ?>
                                <div class="name"><?php echo $articles['name']; ?></div>
                            <?php } ?>
                            <?php if ($articles['button']) { ?>
                                <div class="blog-button"><a
                                            href="<?php echo $articles['href']; ?>"><?php echo $button_more; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="trigger">
            <div class="info_trigger">
                <p><?php echo $pag_results; ?></p>
                <div class="info_trigger-line">
                    <span></span>
                </div>
            </div>
            <div class="ias_trigger">
                <a href="/" class="btn btn-max-default"><?php echo $text_ajax_pp; ?></a>
            </div>
        </div>
        <?php echo $pagination; ?>
    </div>
    <script   >

        $(document).ready(function () {
            var x = Number($(".info_trigger p .p_ins").html());
            var y = Number($(".info_trigger p .p_max").html());
            var x_line ;
            if((x+x)>y){
            //     $(".info_trigger .info_trigger-line span").css('width','100%');
            // }else{
                x_line = x*(100/y);
                $(".info_trigger .info_trigger-line span").css('width',x_line + '%');
            }
            if(x_line==100){
                $('.ias_trigger a').hide();
            }
            $('body').on('click', '.ias_trigger a', function (e) {
                e.preventDefault();
                var tar = $(this);
                $('.pagination li').each(function (i, el) {
                    if ($(el).hasClass('active')) {
                        $(el).removeClass('active');
                        $(el).next().addClass('active');
                        // $("<div class='load-" + i + "'></div>").appendTo('.bnews-list');
                        $("<div class='load'></div>").appendTo('body');
                        $('.load').hide();
                        // console.log($(el).next().find('a').attr('href'));
                        // console.log(encodeURIComponent($(el).next().find('a').attr('href')));
                        // return false;
                        $('.load').load(encodeURI($(el).next().find('a').attr('href')) + ' .bnews-list .box-artblock ', function () {
                            $(".bnews-list").append($('.load>.box-artblock'));
                            var x = Number($(".info_trigger p .p_ins").html());
                            var y = Number($(".info_trigger p .p_max").html());
                            var x_line ;
                            if((x+x)>y){
                                $(".info_trigger p .p_ins").html(y);
                                $(".info_trigger .info_trigger-line span").css('width','100%');
                            }else{
                                x=x+x;
                                x_line = x*(100/y);
                                $(".info_trigger p .p_ins").html(x);
                                $(".info_trigger .info_trigger-line span").css('width',x_line + '%');
                            }
                            $('.load').detach();
                        });
                        // $('#mfilter-content-container').parent('div').find('.pagination').append('<div id="ajaxblock" style="width:' + w + 'px;height:30px;margin-top:20px;text-align:center;border:none !important;"><img src="../image/preload.gif" /></div>');
                        if (i + 4 == $('.pagination li').length) {
                            tar.hide();
                        }
                        return false;
                    }
                });
            });

            $('img.article-image').each(function (index, element) {
                var articleWidth = $(this).parent().parent().width() * 0.7;
                var imageWidth = $(this).width() + 10;
                if (imageWidth >= articleWidth) {
                    $(this).attr("align", "center");
                    $(this).parent().addClass('bigimagein');
                }
            });
            $(window).resize(function () {
                $('img.article-image').each(function (index, element) {
                    var articleWidth = $(this).parent().parent().width() * 0.7;
                    var imageWidth = $(this).width() + 10;
                    if (imageWidth >= articleWidth) {
                        $(this).attr("align", "center");
                        $(this).parent().addClass('bigimagein');
                    }
                });
            });
        });
    </script>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if (!$ncategories && !$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } else { ?>
    <?php if (!$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
    <script   >
        var disqus_shortname = '<?php echo $disqus_sname; ?>';
        (function () {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
    </script>
<?php } ?>
<?php if ($fbcom_status) { ?>
    <script   >
        window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo $fbcom_appid; ?>',
                status: true,
                xfbml: true,
                version: 'v2.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>