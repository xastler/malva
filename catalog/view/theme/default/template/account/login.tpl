<?php echo $header; ?>
    <div class="breadcrumb-block breadcrumb-account">
        <div class="container">
            <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if ($i + 1 < count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                               id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item"
                                  id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i ?>"/>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container">
        <?php if ($success) { ?>
            <script>
                $('.header .container .row').after('<div class="alert popups_block"><div class="popups_block-icon"><i class="icon-shopping-bag"></i></div><div class="popups_block-content"><?php echo $success; ?></div><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            </script>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login_form">
                        <h1><?php echo $text_i_am_returning_customer; ?></h1>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <?php if ($error_warning) { ?>
                                <div class="alert"><?php echo $error_warning; ?></div>
                            <?php } ?>
                            <div class="form-group">
                                <input type="text" name="email" value="<?php echo $email; ?>"
                                       placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" value="<?php echo $password; ?>"
                                       placeholder="<?php echo $entry_password; ?>" id="input-password"
                                       class="form-control"/>
                            </div>
                            <div class="login_form_help">
                                <div class="check_box">
                                    <input type="checkbox" class="checkbox" id="pag-checkbox-l">
                                    <label for="pag-checkbox-l">Показать пароль </label>
                                </div>
                                <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                            </div>

                            <input type="submit" value="<?php echo $button_login; ?>" class="btn"/>
                            <?php if ($redirect) { ?>
                                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                            <?php } ?>
                        </form>
                        <p><a href="javascript:void(0);" title=" " class="quick_login account"><?php echo $text_register; ?></a></p>
                        <p>Вход через соцсети </p>
                        <div class="login_form_social_log">
                            <a href="javascript:void(0);" onclick="social_auth.facebook(this)"
                               data-loading-text="Loading">Facebook</a>
                            <a href="javascript:void(0);" onclick="social_auth.googleplus(this)"
                               data-loading-text="Loading">Google+</a>
                            <a href="javascript:void(0);" onclick="social_auth.instagram(this)"
                               data-loading-text="Loading">instagram</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
    </div>
<?php echo $footer; ?>