<?php echo $header; ?>
<div class="breadcrumb-block">
    <div class="container">
        <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if ($i + 1 < count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                           id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </a>
                    <?php } else { ?>
                        <span itemscope itemtype="http://schema.org/Thing" itemprop="item"
                              id="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    </span>
                    <?php } ?>
                    <meta itemprop="position" content="<?php echo $i ?>"/>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="wishlist_preview js_preview">
    <div class="container">
        <div class="wishlist_preview-container">
            <div class="wishlist_preview-content js_preview-content">
                <h1 class="wishlist_preview-title js_preview-title"><?php echo $heading_title; ?></h1>
            </div>

            <div class="wishlist_preview-img js_preview-img">
                <div class="block_img"></div>
            </div>

        </div>

    </div>

</div>
<div id="content" class="container margin_b_60   ">

    <?php if ($success) { ?>
        <script>
            $('.header .container .row').after('<div class="alert popups_block"><div class="popups_block-icon"><i class="icon-heart"></i></div><div class="popups_block-content"><?php echo $success;?></div><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            setTimeout(function () {
                $('.popups_block').detach();
            }, 5100);
        </script>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <?php echo $content_top; ?>
        <?php if ($products) { ?>
            <?php foreach ($products as $product) { ?>
                <div class="product-layout">
                    <div class="product-thumb transition ">
                        <div class="image">
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-min"/>
                                <img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-min_hover"/>
                                <img src="<?php echo $product['thumb_max']; ?>" alt="<?php echo $product['name']; ?>" class="img-max"/>
                                <img src="<?php echo $product['thumb_max_hover']; ?>" alt="<?php echo $product['name']; ?>" class="img-max_hover"/>
                            </a>
                            <div class="image-label">
                                <?php foreach ($product['labels'] as $label) { ?>
                                    <span><?php echo $label['text']; ?></span><?php } ?>
                            </div>
                            <a href="<?php echo $product['remove']; ?>" class="btn-danger"><i
                                        class="icon-close"></i></a></td>

                            <div class="button-group">
                                <button class="btn-cart" type="button"
                                        onclick="cart.add('<?php echo $product['product_id']; ?>');"><i
                                            class="icon-shopping-bag"></i> <span
                                            class=""><?php echo $button_cart; ?></span></button>
                            </div>
                        </div>

                        <div class="caption">
                            <div class="caption-name"><a
                                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['price']) { ?>
                                <p class="price">
                                    <?php if (!$product['special']) { ?>
                                        <span class="price-new"><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                        <span class="price-new"><?php echo $product['special']; ?></span> <span
                                                class="price-old"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                </p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons clearfix">
                <div class="pull-right"><a href="/" class="btn "><?php echo $button_continue; ?></a></div>
            </div>
        <?php } ?>
        <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?>
</div>
<?php echo $footer; ?>


