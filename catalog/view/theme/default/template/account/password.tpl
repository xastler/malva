<?php echo $header; ?>
    <div class="breadcrumb-block breadcrumb-account">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset class="form-horizontal">
        <div class="form-group required width-100">
            <div class="col-sm-10">
                <input type="password" name="old_password" value="<?php echo $old_password; ?>" placeholder="<?php echo $entry_old_password; ?>" id="input-old_password" class="form-control" />
                <?php if ($error_old_password) { ?>
                    <div class="text-danger"><?php echo $error_old_password; ?></div>
                <?php } ?>
            </div>
        </div>
          <div class="form-group required">
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons">
            <input type="submit" value="<?php echo $button_save; ?>" class="btn" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>