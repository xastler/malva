<?php echo $header; ?>
    <div class="breadcrumb-block breadcrumb-account">
        <div class="container">
            <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if ($i + 1 < count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                               id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item"
                                  id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i ?>"/>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-md-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-md-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-md-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <?php if ($orders) { ?>
                    <?php foreach ($orders as $order) { ?>
                        <a href="<?php echo $order['href']; ?>" class="num-table"><?php echo $text_num_table; ?> <?php echo $order['order_id']; ?></a>
                        <div class="table-responsive">

                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <td><?php echo $column_name; ?></td>
                                    <td><?php echo $column_quantity; ?></td>
                                    <td><?php echo $column_price; ?></td>
                                    <td><?php echo $column_status; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td height="0" colspan="3" class=""
                                        style="padding: 0;    border-color: transparent;"></td>
                                    <td rowspan="100" class="text-center status"
                                        style="vertical-align: inherit;"><?php echo $order['status']; ?></td>
                                </tr>
                                <?php foreach ($order['products_list'] as $product) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $product['name']; ?></td>
                                        <td class="text-center"><?php echo $product['quantity']; ?></td>
                                        <td class="text-center"><?php echo $product['total']; ?></td>

                                    </tr>
                                <?php } ?>

                                <tr>
                                    <td colspan="3" class=""><span
                                                class="pull-left txt"><?php echo $column_total; ?></span><span
                                                class="pull-right total"><?php echo $order['total']; ?></span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    <?php } ?>

                    <div class="text-right"><?php echo $pagination; ?></div>
                <?php } else { ?>
                    <p class="margin_b_60"><?php echo $text_empty; ?></p>
                <?php } ?>
                <!--                <div class="buttons clearfix">-->
                <!--                    <div class="pull-right"><a href="--><?php //echo $continue; ?><!--"-->
                <!--                                               class="btn btn-primary">-->
                <?php //echo $button_continue; ?><!--</a></div>-->
                <!--                </div>-->
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script>
        var myScroll;

        function scroll() {
            myScroll = new IScroll('.checkout-lists', {
                bounce: false,
                scrollbars: true,
                mouseWheel: true,
                interactiveScrollbars: true,
                mouseWheelSpeed: 120
            });
        }

        if ($('.checkout-lists').height() >= 886) {
            scroll();
        }
    </script>
<?php echo $footer; ?>