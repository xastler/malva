<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb-block">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="contact_preview js_preview">
        <div class="container">
            <div class="contact_preview-container">
                <div class="contact_preview-content">
                    <h1 class="contact_preview-title"><?php echo $heading_title; ?></h1>
                    <div class="contact_preview-info">
                        <div class="contact_preview-address"><i class="icon-placeholder"></i><?php echo $address; ?>
                        </div>
                        <div class="contact_preview-tell">
                            <i class="icon-phone-call"></i>
                            <?php if ($telephone) { ?><p><?php echo $text_telephone1 ?> <a
                                    href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone); ?>"><?php echo $telephone; ?></a>
                                </p><?php } ?>
                            <?php if ($telephone) { ?><p><?php echo $text_telephone2 ?> <a
                                    href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2); ?>"><?php echo $telephone2; ?></a>
                                </p><?php } ?>
                            <?php if ($telephone3) { ?><p><?php echo $text_telephone3 ?> <a
                                    href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3); ?>"><?php echo $telephone3; ?></a>
                                </p><?php } ?>
                        </div>
                        <?php if ($x_email) { ?>
                            <div class="contact_preview-mail"><i class="icon-email"></i><a
                                    href="mail:<?php echo $x_email; ?>"><?php echo $x_email; ?></a></div><?php } ?>
                    </div>
                </div>
                <div class="contact_preview-img js_preview-img">
                    <img src="<?php echo $image; ?>" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container"><?php echo $content_top; ?>
        <?php if ($locations) { ?>
            <h3><?php echo $text_store; ?></h3>
            <div class="panel-group" id="accordion">
                <?php foreach ($locations as $location) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>"
                                                       class="accordion-toggle" data-toggle="collapse"
                                                       data-parent="#accordion"><?php echo $location['name']; ?> <i
                                            class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div class="panel-collapse collapse"
                             id="collapse-location<?php echo $location['location_id']; ?>">
                            <div class="panel-body">
                                <div class="row">
                                    <?php if ($location['image']) { ?>
                                        <div class="col-sm-3"><img src="<?php echo $location['image']; ?>"
                                                                   alt="<?php echo $location['name']; ?>"
                                                                   title="<?php echo $location['name']; ?>"
                                                                   class="img-thumbnail"/></div>
                                    <?php } ?>
                                    <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br/>
                                        <address>
                                            <?php echo $location['address']; ?>
                                        </address>
                                        <?php if ($location['geocode']) { ?>
                                            <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15"
                                               target="_blank" class="btn btn-info"><i
                                                        class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-3"><strong><?php echo $text_telephone; ?></strong><br>
                                        <?php echo $location['telephone']; ?><br/>
                                        <br/>
                                        <?php if ($location['fax']) { ?>
                                            <strong><?php echo $text_fax; ?></strong><br>
                                            <?php echo $location['fax']; ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php if ($location['open']) { ?>
                                            <strong><?php echo $text_open; ?></strong><br/>
                                            <?php echo $location['open']; ?><br/>
                                            <br/>
                                        <?php } ?>
                                        <?php if ($location['comment']) { ?>
                                            <strong><?php echo $text_comment; ?></strong><br/>
                                            <?php echo $location['comment']; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <form  method="post" enctype="multipart/form-data" class="contact_form" id="contact_form">
            <fieldset>
                <legend><?php echo $text_contact; ?></legend>
                <div class="contact_form-inputs">
                    <div class="contact_form-input">
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name_contact"
                               class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                    </div>
                    <div class="contact_form-input">
                        <input type="text" name="telephone" value="" id="input-phone_contact"
                               class="form-control" placeholder="<?php echo $entry_phone; ?>"/>
                    </div>
                    <div class="contact_form-input">
                        <input type="text" name="email" value="<?php echo $email; ?>" id="input-email_contact"
                               class="form-control" placeholder="<?php echo $entry_email; ?>"/>
                    </div>
                </div>
                <div class="contact_form-textarea">
                        <textarea name="enquiry" rows="10" id="input-enquiry_contact"
                                  class="form-control"
                                  placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
            </fieldset>
            <div class="contact_form-buttons">
                <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>"/>
            </div>
        </form>
    </div>
    <div id="map" class="contact_map" data-point="<?php echo $data['map-point']; ?>">

    </div>
    <script>
        if($('#map').length){
            init($('#map').data('point'));
        }
        function init(point) {
            var point_y = point.slice(0, point.indexOf(','));
            var point_x = point.slice(point.indexOf(' '),point.length);
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(point_y, point_x),
                styles:[
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#FF007B"
                            },
                            {
                                "saturation": 59.80000000000001
                            },
                            {
                                "lightness": 21
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#FF00AF"
                            },
                            {
                                "saturation": 32.599999999999994
                            },
                            {
                                "lightness": 20.599999999999994
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#FFAF00"
                            },
                            {
                                "lightness": 50.80000000000001
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#FFE800"
                            },
                            {
                                "lightness": 8.600000000000009
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#FFD900"
                            },
                            {
                                "saturation": 44.79999999999998
                            },
                            {
                                "lightness": 3.6000000000000085
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#0078FF"
                            },
                            {
                                "saturation": 24.200000000000003
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    }
                ]
            };
            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(point_y, point_x),
                map: map,
                title: 'Augusta Apartments\n' +
                    'Eichstrasse 6\n' +
                    '76530 Baden-Baden'


            });
        }
        $('#contact_form').submit(function() {
            $.ajax({
                url: 'index.php?route=information/contact/form_x',
                type: 'post',
                data: $('#contact_form input[type=\'text\'], #contact_form textarea[name=\'enquiry\']'),
                dataType: 'json',
                beforeSend: function() {
                    $('#contact_form .btn').button('loading');
                },
                complete: function() {
                    $('#contact_form .btn').button('reset');
                },
                success: function(json) {
                    $('#contact_form .text-danger').remove();
                    $('#contact_form input').removeClass('error_input');
                    $('#contact_form textarea').removeClass('error_input');
                    if (json['name']) {
                        $('#contact_form #input-name_contact').after('<div class="text-danger">' + json['name'] + '</div>');
                    }
                    if (json['email']) {
                        $('#contact_form #input-email_contact').after('<div class="text-danger">' + json['email'] + '</div>');
                    }
                    if (json['enquiry']) {
                        $('#contact_form #input-enquiry_contact').after('<div class="text-danger">' + json['enquiry'] + '</div>');
                    }

                    if(json['success']){
                        $('#contact_form .text-danger').remove();
                        $('#contact_form input').removeClass('error_input');
                        $('#contact_form textarea').removeClass('error_input');
                        $('#contact_form input,#contact_form textarea').val('');
                        $('.header .container .row').after('<div class="user_message_wrapp alert"> ' + json['text_message'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        setTimeout(function () {
                            $('.alert').detach();
                        }, 5100);

                    }

                }
            });
            return false;
        });

    </script>
    <div class="container">
        <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
    <address style="display: none">
        <div itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="name"><?php echo $store ?></span><br/>
            <?php echo $address; ?>
            Телефон: <span itemprop="telephone"><?php echo $telephone ?></span><br/>
            E-mail: <span itemprop="email"><?php echo $x_email ?></span>
        </div>
        <?php //echo $address; ?>
    </address>
</div>
<?php echo $footer; ?>
