<?php echo $header; ?>
    <div class="breadcrumb-block">
        <div class="container">
            <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if ($i + 1 < count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                               href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                        <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div id="content">
        <?php /*<h1><?php echo $heading_title; ?></h1> */ ?>
        <div class="about_preview js_preview">
            <div class="container">
                <div class="about_preview-container">
                    <div class="about_preview-content js_preview-content">
                        <h1 class="about_preview-title js_preview-title"><?php echo $heading_title; ?></h1>

                        <div class="about_preview-info">
                            <?php echo $description_top; ?>
                        </div>

                    </div>

                    <div class="about_preview-img js_preview-img">
                        <img src="<?php echo $thumb; ?>" alt="">
                    </div>

                </div>

            </div>

        </div>
        <?php echo $description; ?>


    </div>
    <div class="container">
        <?php echo $content_bottom; ?>
    </div>

<?php echo $footer; ?>

