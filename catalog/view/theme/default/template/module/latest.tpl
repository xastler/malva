<div class="new_products">
    <div class="container">
        <div class="new_products-name"><?php echo $heading_title; ?></div>
        <div class="row">
            <?php $i=1; foreach ($products as $product) { ?>
                <?php if($i<=6) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>
                <?php  } ?>
            <?php $i++; } ?>
        </div>
        <a href="<?php echo $page_new; ?>" class="btn"><?php echo $text_btn; ?></a>
    </div>
</div>