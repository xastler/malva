
<div class="slider-home">
    <div id="slideshow<?php echo $module; ?>" class="slider-home-slide" style="opacity: 1;">
      <?php foreach ($banners as $banner) { ?>
      <div class="item" style="background-image: url('<?php echo $banner['image']; ?>')">
        <?php if ($banner['link']) { ?>
            <div class="container">
                <div class="item-block">
                    <div class="item-block-bg" style="background-image: url('<?php echo $banner['image']; ?>')"></div>
                    <div class="item-block-content">
                        <div class="item-block-txt1"><?php echo $banner['description_1']; ?></div>
                        <p class="item-block-txt2"><?php echo $banner['description_2']; ?></p>
                        <p class="item-block-txt3"><?php echo $banner['description_3']; ?></p>
                    </div>
                    <a href="<?php echo $banner['link']; ?>" class="btn"><?php echo $banner['btn_text']; ?></a>
                </div>
            </div>
        <?php } else { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <div class="slider-home-dots">
        <div class="container"></div>
    </div>
</div>
<script   >
$('#slideshow<?php echo $module; ?>').slick({
    dots: true,
    infinite: true,
    fadeSpeed: 1000,
    arrows:false,
    appendDots:$('.slider-home-dots .container'),
    fade: true,
    autoplay:true
});
</script>