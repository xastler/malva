<div class="home-news">
    <div class="container">
        <div class="home-news-title"><?php echo $heading_title; ?></div>
        <div class="home-news-slider">
            <div class="news-prev"><i class="icon-down-arrow"></i></div>
            <div class="home-news-slick bnews-list<?php if ($display_style) { ?> bnews-list-2<?php } ?>">
                <?php foreach ($article as $articles) { ?>
                    <div class="artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">
                        <?php if ($articles['thumb']) { ?>
                            <a href="<?php echo $articles['href']; ?>">
                                <img class="article-image"   src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>"
                                     alt="<?php echo $articles['name']; ?>, <?php echo $text_photo; ?>" />
                            </a>
                        <?php } ?>
                        <div class="artblock-content">

                            <div class="article-meta">
                                <?php if ($articles['date_added']) { ?>
                                    <?php echo $articles['date_added']; ?>
                                <?php } ?>
                            </div>
                            <?php if ($articles['name']) { ?>
                                <div class="name"><?php echo $articles['name']; ?></div>
                            <?php } ?>
                            <?php if ($articles['button']) { ?>
                                <div class="blog-button"><a href="<?php echo $articles['href']; ?>"><?php echo $button_more; ?></a></div>
                            <?php } ?>
                        </div>

                    </div>
                <?php } ?>
            </div>
            <div class="news-next"><i class="icon-down-arrow"></i></div>
        </div>
        <a href="<?php echo $newslink; ?>" class="btn home-news-btn"><?php echo $text_btn; ?></a>
    </div>
</div>



<script   >
    $('.home-news-slick').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: $('.news-prev'),
        nextArrow: $('.news-next'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
	$(document).ready(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
	});
</script>
<?php if ($disqus_status) { ?>
<script   >
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script   >
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>