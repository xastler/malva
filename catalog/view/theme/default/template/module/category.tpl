<h2 class="hidden-sm hidden-xs"><?php echo $heading_title; ?></h2>
<div class="category_list hidden-sm hidden-xs">
    <ul>
        <?php foreach ($categories as $category) { ?>
            <li class="<?php if ($category['category_id'] == $category_id) { ?> active<?php } ?>">

                <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                <?php if ($category['children']) { ?>
                    <i class="icon-down-arrow"></i>
                    <ul>
                        <?php foreach ($category['children'] as $child) { ?>
                            <li>
                                <?php if ($child['category_id'] == $child_id) { ?>
                                    <a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        </ul>
                <?php } ?>
            </li>
        <?php } ?>
        <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
    </ul>
</div>
