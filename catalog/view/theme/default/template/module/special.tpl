<div class="wrapper_special">
    <div class="container">
        <div class="wrapper_special-title"><?php echo $heading_title; ?></div>
        <div class="wrapper_special-slider">
            <div class="special-prev"><i class="icon-down-arrow"></i></div>
            <div class="wrapper_special-slick">
              <?php foreach ($products as $product) { ?>
                  <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
              <?php } ?>
            </div>
            <div class="special-next"><i class="icon-down-arrow"></i></div>
        </div>
        <a href="<?php echo $special; ?>" class="btn wrapper_special-btn"><?php echo $text_btn; ?></a>
    </div>
</div>

<script>
    $('.wrapper_special-slick').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: $('.wrapper_special .special-prev'),
        nextArrow: $('.wrapper_special .special-next'),
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 671,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>