<?php if(isset($products)){ ?>
    <div class="watched_products">
        <div class="container">
            <h2><?php echo $heading_title; ?></h2>
            <div class="watched_products-slider">
                <div class="special-prev"><i class="icon-down-arrow"></i></div>
                <div class="watched_products-slick">
                    <?php foreach ($products as $product) { ?>
                        <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'button_details' => $button_details));?>
                    <?php } ?>
                </div>
                <div class="special-next"><i class="icon-down-arrow"></i></div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    $('.watched_products-slick').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: $('.watched_products .special-prev'),
        nextArrow: $('.watched_products .special-next'),
        responsive: [

            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 671,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            ]
    });
</script>
