<div class="account-menu">
    <ul>
        <?php if (!$logged) { ?>
            <li <?php if($og_url==$login){?> class="active"<?php } ?>><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <li <?php if($og_url==$register){?> class="active"<?php } ?>><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li <?php if($og_url==$forgotten){?> class="active"<?php } ?>><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
        <?php } ?>
        <?php if ($logged) { ?>
            <li <?php if($og_url==$edit){?> class="active"<?php } ?>><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
            <li <?php if($og_url==$password){?> class="active"<?php } ?>><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
        <?php } ?>
        <li <?php if($og_url==$order){?> class="active"<?php } ?>><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <?php if ($logged) { ?>
            <li <?php if($og_url==$logout){?> class="active"<?php } ?>><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
        <?php } ?>
    </ul>
</div>

