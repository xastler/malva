<div class="html_text">
    <?php if ($heading_title) { ?>
        <h1><?php echo $heading_title; ?></h1>
    <?php } ?>
    <?php if ($status_btn == 1) { ?>
        <div class="home_about">
            <div class="container">
                <div class="home_about-content">
                    <?php echo $html; ?>
                </div>
                <div class="home_about-open" style="text-align: center; "><?php echo $txt_status_btn; ?></div>
            </div>
        </div>
    <?php } else { ?>
        <?php echo $html; ?>
    <?php } ?>
</div>
