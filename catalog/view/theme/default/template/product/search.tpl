<?php echo $header; ?>
<?php //$this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <div class="breadcrumb-block breadcrumb-account">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<div class="container">
    <h1><?php echo $heading_title; ?></h1>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
          <div class="catalog_sort">
              <div class="catalog_sort-btn"><?php echo $text_sort; ?><i class="icon-down-arrow"></i></div>
              <ul>
                  <li><?php echo $text_sort; ?></li>
                  <?php foreach ($sorts as $sorts) { ?>
                      <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                          <li class="active"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                      <?php } else { ?>
                          <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                      <?php } ?>
                  <?php } ?>
              </ul>
          </div>
        <div class="products">
            <div class="row">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>

                <?php } ?>
            </div>
      </div>
          <div class="prod_trigger">
              <div class="prod_trigger-info">
                  <p><?php echo $pag_results; ?></p>
                  <div class="prod_trigger-line">
                      <span></span>
                  </div>
              </div>
              <div class="prod_trigger-a">
                  <a href="/" class="btn btn-max-default "><?php echo $text_ajax_pp; ?></a>
              </div>
          </div>

          <div class="row">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          </div>
      <?php } else { ?>
      <p class="margin_b_60"><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script   ><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>