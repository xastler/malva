<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
<?php //echo $column_left; ?>
<div id="content" class="page_product" itemscope itemtype="http://schema.org/Product">
    <?php echo $content_top; ?>
    <div class="product_preview" id="product">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="product_preview-name"><?php echo $heading_title; ?></div>
                    <div class="product_preview-img">
                        <?php if ($thumb) { ?>
                            <img
                                    itemprop="image" class="product_preview-img_main" src="<?php echo $thumb; ?>"
                                    title="<?php echo $heading_title; ?>, <?php echo $text_art; ?>: <?php echo $sku; ?>"
                                    alt="<?php echo $heading_title; ?> (<?php echo $sku; ?>) <?php echo $text_photo; ?> 1"/>
                        <?php } ?>

                            <ul class="thumbnails">
                                <?php if ($thumb) { ?>
                                    <li>
                                        <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img
                                                    itemprop="image" src="<?php echo $thumb; ?>"
                                                    title="<?php echo $heading_title; ?>, <?php echo $text_art; ?>: <?php echo $sku; ?>"
                                                    alt="<?php echo $heading_title; ?> (<?php echo $sku; ?>) <?php echo $text_photo; ?> 1"/></a>
                                    </li>
                                <?php } ?>
                                <?php $k=2; foreach ($images as $image) { ?>
                                    <li class="image-additional"><a class="123"
                                                                    href="<?php echo $image['popup']; ?>"
                                                                    title="<?php echo $heading_title; ?>"> <img
                                                    src="<?php echo $image['thumb']; ?>"
                                                    title="<?php echo $heading_title; ?>, <?php echo $text_art; ?>: <?php echo $sku; ?>"
                                                    alt="<?php echo $heading_title; ?> (<?php echo $sku; ?>) <?php echo $text_photo; ?> <?php echo $k; ?>"/></a></li>
                                <?php $k++;} ?>
                            </ul>
                        
<!--                        --><?php //if($special_text){ ?><!--<span class="special_text">--><?php //echo $special_text; ?><!--</span>--><?php //} ?>
                        <?php if($labels){ ?>
                            <div class="special_text">
                                <?php foreach ($labels as $label ) { ?><span class="image-label"><?php echo $label['text']; ?></span><?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product_preview-content">
                        <div class="top-info">
                            <div class="article" itemprop="sku">Арт. <?php echo $sku; ?></div>
                            <div class="availability">
                                <span ><?php echo $stock; ?></span>
                            </div>
                        </div>
                        <h1 itemprop="name"><?php echo $heading_title; ?></h1>
                        <?php if ($price) { ?>
                            <div class="price">
                                <?php if (!$special) { ?>
                                    <span class="new" ><?php echo $price; ?></span>
                                <?php } else { ?>
                                    <span class="new"><?php echo $special; ?></span>
                                    <span class="old"><?php echo $price; ?></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="attribute">
                            <ul>
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <li>
                                            <span><?php echo $attribute['name']; ?>: </span><?php echo $attribute['text']; ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="options">
                            <div class="options-select">
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <div class="<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                    id="input-option<?php echo $option['product_option_id']; ?>">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="options-num">
                                <span class="num minus">
                                    <i class="icon-down-arrow"></i>
                                </span>
                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2"
                                       id="input-quantity" class="form-control" maxlength="2"/>
                                <span class="num plus">
                                    <i class="icon-down-arrow"></i>
                                </span>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                            </div>
                        </div>
                        <div class="buttons">

                            <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"
                                    class="btn btn-min-default"><i
                                        class="icon-shopping-bag"></i> <?php echo $button_cart; ?></button>
                            <button type="button" class="btn-wishlist <?php echo ($wishlist_status)?'active':''; ?>"
                                    onclick="wishlist.add('<?php echo $product_id; ?>');"><i
                                        class="icon-heart"></i><?php echo $text_wishlist_btn; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_tabs">
        <div class="container">
            <ul class="product_tabs-name">
                <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                <?php if ($attribute_groups) { ?>
                    <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_info; ?></a></li>
                <?php } ?>
            </ul>
            <div class="product_tabs-content tab-content">
                <div class="tab-pane active tab-description" id="tab-description"
                     itemprop="description">
                    <div class="tab-btn-mob"><?php echo $tab_description; ?><i class="icon-down-arrow"></i></div>
                    <div class="tab-box">
                        <div class="row">
                            <div class="col-md-4">
                                <?php if ($description) { ?>
                                    <div class="tab-description-title">
                                        <?php echo $tab_attribute; ?>
                                    </div>
                                    <div class="tab-description-content">
                                        <?php echo $description; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-4">
                                <?php if ($recommendations) { ?>
                                    <div class="tab-description-title">
                                        <?php echo $tab_recommendations; ?>
                                    </div>
                                    <div class="tab-description-content">
                                        <?php echo $recommendations; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-4">
                                <?php if ($storage) { ?>
                                    <div class="tab-description-title">
                                        <?php echo $tab_storage; ?>
                                    </div>
                                    <div class="tab-description-content">
                                        <?php echo $storage; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($information) { ?>
                    <div class="tab-pane" id="tab-specification">
                        <div class="tab-btn-mob"><?php echo $tab_info; ?><i class="icon-down-arrow"></i></div>
                        <div class="tab-box">
                            <?php echo $information; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>
<?php //echo $column_right; ?>
<?php if ($products) { ?>
<div class="products_related">
    <div class="container">
        <h2><?php echo $text_related; ?></h2>
        <div class="wrapper_special-slider">
            <div class="special-prev"><i class="icon-down-arrow"></i></div>
            <div class="wrapper_special-slick">
                <?php $i = 0; ?>
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>
                <?php } ?>
            </div>
            <div class="special-next"><i class="icon-down-arrow"></i></div>
        </div>
    </div>
</div>
<?php } ?>
<?php if ($tags) { ?>
    <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
            <?php if ($i < (count($tags) - 1)) { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
            <?php } else { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
            <?php } ?>
        <?php } ?>
    </p>
<?php } ?>
<?php echo $content_bottom; ?>

<script   >
    $('.wrapper_special-slick').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: $('.products_related .special-prev'),
        nextArrow: $('.products_related .special-next'),
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 671,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    if($(window).width()<600){
        $('.product_preview-img ul').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }
</script>
<script   ><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script   ><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {

                    $('.header .container .row').after('<div class="alert popups_block"><div class="popups_block-icon"><i class="icon-shopping-bag"></i></div><div class="popups_block-content">' + json['success'] + '</div><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    setTimeout(function () {
                        $('.popups_block').detach();
                    }, 3100);

                    $('#cart span').html(json['total']);

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script   ><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script   ><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    // $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                    $('.header .container .row').after('<div class="alert popups_block"><div class="popups_block-icon"><i class="icon-shopping-bag"></i></div><div class="popups_block-content">' + json['success'] + '</div><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    setTimeout(function () {
                        $('.popups_block').detach();
                    }, 3100);

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>
