<?php echo $header; ?>
<?php //$this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <div class="breadcrumb-block">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="catalog_preview js_preview">
        <div class="container">
            <div class="catalog_preview-container">
                <div class="catalog_preview-content js_preview-content">
                    <h1 class="catalog_preview-title js_preview-title"><?php echo $heading_title; ?></h1>
                    <?php if (0) { ?>
                        <div class="catalog_preview-description"><?php echo $description; ?></div>
                    <?php } ?>
                </div>
                <div class="catalog_preview-img js_preview-img">
                    <?php if ($thumb_special) { ?>
                        <img src="<?php echo $thumb_special; ?>" alt="<?php echo $heading_title; ?>"
                             title="<?php echo $heading_title; ?>"/>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
          <div class="catalog_sort">
              <div class="catalog_sort-btn"><?php echo $text_sort; ?><i class="icon-down-arrow"></i></div>
              <ul>
                  <li><?php echo $text_sort; ?></li>
                  <?php foreach ($sorts as $sorts) { ?>
                      <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                          <li class="active"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                      <?php } else { ?>
                          <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                      <?php } ?>
                  <?php } ?>
              </ul>
          </div>
          <div class="megafilter-btn"><?php echo $text_filter_btn; ?><i class="icon-settings"></i></div>
    <div class="products">
      <div class="row">
        <?php foreach ($products as $product) { ?>
            <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>

        <?php } ?>
      </div>

    </div>
          <div class="prod_trigger">
              <div class="prod_trigger-info">
                  <p><?php echo $pag_results; ?></p>
                  <div class="prod_trigger-line">
                      <span></span>
                  </div>
              </div>
              <div class="prod_trigger-a">
                  <a href="/" class="btn btn-max-default "><?php echo $text_ajax_pp; ?></a>
              </div>
          </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>