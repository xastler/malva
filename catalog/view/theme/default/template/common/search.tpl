<div id="search" class="search">
    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control" />
    <button type="button"><i class="icon-magnifying-glass"></i></button>
</div>