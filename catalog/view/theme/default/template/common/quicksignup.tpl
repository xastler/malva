
<div class="modal fade modal_ac" id="modal-ac" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
		<div class="modal-content" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php echo $text_returning ?></h4>
			</div>
			<div class="modal-body">
                <div class="modal_login" id="modal-login">
                    <input type="text" name="email" value=""  id="input-email-l" class="form-control" placeholder="<?php echo $entry_email; ?>" />
                    <input type="password" name="password" value="" id="input-password-l" class="form-control" placeholder="<?php echo $entry_password; ?>" />
                    <div class="modal_help">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox-l" />
                            <label for="checkbox-l"><?php echo $text_open_pass; ?></label>
                        </div>
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                    </div>
                    <button type="button" class="btn loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
                    <p><?php echo $text_login_social; ?></p>
                    <div class="modal_social_log">
                        <a href="javascript:void(0);" onclick="social_auth.facebook(this)" data-loading-text="Loading">Facebook</a>
                        <a href="javascript:void(0);" onclick="social_auth.googleplus(this)" data-loading-text="Loading">Google+</a>
                        <?php /*<a href="javascript:void(0);" onclick="social_auth.instagram(this)" data-loading-text="Loading">instagram</a>*/?>
                    </div>
                </div>
                <div class="modal_register" id="modal-register">
                    <input type="text" name="email" value="" id="input-email-r" class="form-control" placeholder="<?php echo $entry_email; ?>" />
                    <input type="password" name="password" value="" id="input-password-r" class="form-control" placeholder="<?php echo $entry_password; ?>" />
                    <input type="text" name="name" value="" id="input-name-r" class="form-control" placeholder="<?php echo $entry_name; ?>" />
                    <input type="text" name="surname" value="" id="input-surname-r" class="form-control" placeholder="<?php echo $entry_surname; ?>" />
                    <input type="text" name="telephone" value="" id="input-telephone-r" class="hide"/>
                    <div class="modal_help">
                        <div class="check_box">
                            <input type="checkbox" class="checkbox" id="checkbox-r" />
                            <label for="checkbox-r"><?php echo $text_open_pass; ?></label>
                        </div>

                    </div>
                    <button type="button" class="btn btn-max-st2 createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_register; ?></button>
                </div>
			</div>
		</div>
	</div>
</div>


<script   ><!--
$(document).delegate('.quick_login', 'click', function(e) {
	$('#modal-ac').modal('show');
});
//--></script>
<script   ><!--
$('#modal-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#modal-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#modal-register input[type=\'text\'], #modal-register input[type=\'password\'], #modal-register input[type=\'checkbox\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#modal-register .createaccount').button('loading');
			$('#modal-register .alert').remove();
		},
		complete: function() {
			$('#modal-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#modal-register input').removeClass('has-error');

			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}


			if (json['error_surname']) {
				$('#modal-register #input-surname-r').addClass('has-error');
				$('#modal-register #input-surname-r').focus();
			}

			if (json['error_name']) {
                $('#modal-register #input-name-r').addClass('has-error');
                $('#modal-register #input-name-r').focus();
            }

			if (json['error_password']) {
				$('#modal-register #input-password-r').addClass('has-error');
				$('#modal-register #input-password-r').focus();
			}

            if (json['error_email']) {
                $('#modal-register #input-email-r').addClass('has-error');
                $('#modal-register #input-email-r').focus();
            }
			if (json['error']) {
				$('#modal-register .modal_help ').after('<div class="alert">' + json['error'] + '</div>');
			}

			//if (json['now_login']) {
			//	$('.quick-login').before('<li class="dropdown"><a href="<?php //echo $account; ?>//" title="<?php //echo $text_account; ?>//" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php //echo $text_account; ?>//</span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php //echo $account; ?>//"><?php //echo $text_account; ?>//</a></li><li><a href="<?php //echo $order; ?>//"><?php //echo $text_order; ?>//</a></li><li><a href="<?php //echo $transaction; ?>//"><?php //echo $text_transaction; ?>//</a></li><li><a href="<?php //echo $download; ?>//"><?php //echo $text_download; ?>//</a></li><li><a href="<?php //echo $logout; ?>//"><?php //echo $text_logout; ?>//</a></li></ul></li>');
            //
			//	$('.quick-login').remove();
			//}
			if (json['success']) {
			    console.log(json['heading_title']);
				$('#modal-ac .modal-title').html(json['heading_title']);
				success = '<div class="modal_end"><div>'+ json['text_message'] +' </div><div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn">'+ json['button_continue'] +'</a></div></div>';
				$('#modal-ac .modal-body').html(success);
			}
		}
	});
});
//--></script>
<script   ><!--
$('#modal-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#modal-login .loginaccount').trigger('click');
	}
});
$('#modal-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#modal-login input[type=\'text\'], #modal-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#modal-login .loginaccount').button('loading');
			$('#modal-login .alert').remove();
		},
		complete: function() {
			$('#modal-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-login .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}

			if (json['error']) {
				$('#modal-login .modal_help').after('<div class="alert">' + json['error'] + '</div>');
				$('#modal-login #input-email-l').addClass('has-error');
				$('#modal-login #input-password-l').addClass('has-error');
				$('#modal-login #input-email-l').focus();
			}
			if(json['success']){
				loacation();
				$('#modal-login').modal('hide');
			}

		}
	});
});
//--></script>
<script   ><!--
function loacation() {
	location.reload();
}
//--></script>
