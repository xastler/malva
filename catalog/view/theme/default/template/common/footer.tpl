<footer class="footer">
  <div class="container">
      <div class="row">
          <?php if ($informations) { ?>
              <div class="col-sm-3">
                  <h5><?php echo $text_information; ?><i class="icon-down-arrow"></i></h5>
                  <ul class="list-unstyled">
                      <?php $i=1; foreach ($informations as $information) {  ?>
                          <?php if($i<=1){ ?> <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li><?php } ?>
                      <?php $i++;} ?>
                      <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
                      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                  </ul>
              </div>
          <?php } ?>
          <div class="col-sm-3">
              <h5><?php echo $text_catalog; ?><i class="icon-down-arrow"></i></h5>
              <ul class="list-unstyled">
                  <?php foreach ($categories as $category) {  ?>
                      <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                  <?php } ?>
                  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
              </ul>
          </div>
          <div class="col-sm-3">
              <h5><?php echo $text_extra; ?><i class="icon-down-arrow"></i></h5>
              <ul class="list-unstyled">

                  <?php foreach ($articles as $article) {  ?>
                      <li><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></li>
                  <?php } ?>
              </ul>
          </div>
          <div class="col-sm-3">
              <h5><?php echo $text_contact; ?><i class="icon-down-arrow"></i></h5>
              <div class="footer-contact">
              <?php if ($telephone) { ?><div class="footer-address"><?php echo $address; ?></div><?php }?>
              <?php if ($telephone) { ?><div class="footer-tel"><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>"><?php echo $telephone; ?></a></div><?php } ?>
              <?php if ($email) { ?><div class="footer-mail"><a href="mail:<?php echo $email; ?>"><?php echo $email; ?></a></div><?php } ?>
              </div>
          </div>
      </div>
  </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="copyright-content">
            <div class="copyright-content-text"> <?php echo $powered; ?></div>
            <a href="https://web-systems.solutions/" class="copyright-content-img" target="_blank">
            </a>
        </div>
    </div>
</div>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>