<?php echo $header; ?>
    <div class="breadcrumb-block breadcrumb-account">
        <div class="container">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                            </a>
                        <?php } else { ?>
                            <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        </span>
                        <?php } ?>
                        <meta itemprop="position" content="<?php echo $i?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

<div class="container">
  <div class="row">
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn margin_b_60"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>