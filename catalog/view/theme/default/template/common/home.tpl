<?php echo $header; ?>
<?php echo $column_left; ?>
    <div id="content">
        <?php echo $content_top; ?>
        <ul class="baner-plus">
            <li><i class="icon-award"></i>  <?php echo $text_baner_plus_1; ?></li>
            <li><i class="icon-towel"></i>  <?php echo $text_baner_plus_2; ?></li>
            <li><i class="icon-wallet"></i> <?php echo $text_baner_plus_3; ?></li>
            <li><i class="icon-package"></i><?php echo $text_baner_plus_4; ?></li>
        </ul>
        <div class="banner_category">
            <div class="row">
                <?php $k=1; foreach ($categories as $category) {  ?>
                    <div class="col-lg-3 col-md-3">
                        <div class="banner_category-content bg_cagteg_<?php echo $k; ?>" style="background-image: url('<?php echo $category['image']; ?>'),linear-gradient(238deg, #fcf2f4, #f5899c)" >
                            <img src="<?php echo $category['image']; ?>" alt="">
                            <div class="banner_category-text">
                                <div class="banner_category-name"><?php echo $category['name']; ?></div>
                                <a href="<?php echo $category['href']; ?>"><?php echo $text_open; ?></a>
                            </div>
                        </div>
                    </div>
                <?php $k++; } ?>
            </div>
            <style>
                <?php $i=1; foreach ($categories as $category) {  ?>
                .banner_category-content.bg_cagteg_<?php echo $i; ?>:hover{
                    background-image: url('<?php echo $category['image']; ?>'),linear-gradient(transparent,transparent)!important;
                }
                <?php $i++;} ?>
            </style>
        </div>
        <?php echo $content_bottom; ?>
        <?php if(1) { ?>
            <div class="instagram_wrapper">
                <div class="container">
                    <div class="instagram_wrapper-title">#dreamstextile</div>
                </div>
                <div class="instagram_list">
<!--                    --><?php
//
//                    #require_once 'inwidget/classes/Autoload.php';
//                    require_once 'inwidget/classes/InstagramScraper.php';
//                    require_once 'inwidget/classes/Unirest.php';
//                    require_once 'inwidget/classes/InWidget.php';
//
//                    try {
//                        $inWidget = new \inWidget\Core;
//                        $inWidget->getData();
//                        include 'inwidget/template.php';
//                    }
//                    catch (\Exception $e) {
//                        echo $e->getMessage();
//                    }
//
//                    ?>
                    <?php #require_once 'inwidget/classes/Autoload.php';
                    require_once 'inwidget/classes/InstagramScraper.php';
                    require_once 'inwidget/classes/Unirest.php';
                    require_once 'inwidget/classes/InWidget.php';

                    try {

                        // Options may change through class constructor. For example:

                        $config = array(
                            'LOGIN' => 'DREAMSTEXTILE',
                            'HASHTAG' => '',
                            'ACCESS_TOKEN' => '',
                            'authLogin' => '',
                            'authPassword' => '',
                            'tagsBannedLogins' => '',
                            'tagsFromAccountOnly' => false,
                            'imgRandom' => false,
                            'imgCount' => 30,
                            'cacheExpiration' => 6,
                            'cacheSkip' => false,
                            'cachePath' =>  $_SERVER['DOCUMENT_ROOT'].'/inwidget/cache/',
                            'skinDefault' => 'default',
                            'skinPath'=> '/inwidget/skins/',
                            'langDefault' => 'ru',
                            'langAuto' => false,
                            'langPath' => $_SERVER['DOCUMENT_ROOT'].'/inwidget/langs/',
                        );
                        $inWidget = new \inWidget\Core($config);

                        // Also, you may change default values of properties

                        /*
                        $inWidget->width = 800;	// widget width in pixels
                        $inWidget->inline = 6; // number of images in single line
                        $inWidget->view = 18; // number of images in widget
                        $inWidget->toolbar = false;	// show profile avatar, statistic and action button
                        $inWidget->preview = 'large'; // quality of images: small, large, fullsize
                        $inWidget->adaptive = false; // enable adaptive mode
                        $inWidget->skipGET = true; // skip GET variables to avoid name conflicts
                        $inWidget->setOptions(); // apply new values
                        */

                        $inWidget->getData();
                        include 'inwidget/template.php';

                    }
                    catch (\Exception $e) {
                        echo $e->getMessage();
                    }?>
                </div>
            </div>
            <script>
                $('.instagram_list').slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 7,
                    slidesToScroll: 7,
                    responsive: [
                        {
                            breakpoint: 1500,
                            settings: {
                                slidesToShow: 5,
                                slidesToScroll: 5
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 4
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 425,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

                if($(window).width()<=1024){
                    $('.banner_category .row').slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        responsive: [
                            {
                                breakpoint: 992,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }
                if($(window).width()<=992){
                    $('.new_products .row').slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 671,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                    $('.baner-plus').slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        responsive: [
                            {
                                breakpoint: 560,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }
            </script>
        <?php } ?>
    </div>
<?php echo $column_right; ?>

<?php echo $footer; ?>


