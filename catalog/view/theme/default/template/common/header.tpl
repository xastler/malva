<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php if ($lang == 'ua') {
    $lang = 'uk';
}
echo $lang; ?>">
<!--<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" href="/reject/reject.css" media="all"/>
<script src="/reject/reject.min.js"></script>
<![endif]-->
<head itemscope>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($meta) { ?>
        <meta name="robots" content="<?php echo $meta; ?>"/>
    <?php } ?>
    <?php if (isset($robots) && $robots) { ?>
        <meta name="robots" content="<?php echo $robots; ?>"/>
    <?php } ?>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>

    <meta property="url" content="<?php echo $og_url; ?>"/>
    <meta property="title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <?php if ($og_image) { ?>
        <meta property="image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="image" content="<?php echo $logo; ?>"/>
    <?php } ?>

    <meta itemprop="url" content="<?php echo $og_url; ?>">
    <meta itemprop="name" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <?php if ($og_image) { ?>
        <meta itemprop="image" content="<?php echo $og_image; ?>">
    <?php } else { ?>
        <meta itemprop="image" content="<?php echo $logo; ?>">
    <?php } ?>

    <?php if ($gtm) { ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?
                id = '+i+dl;f.parentNode.insertBefore(j,f);
            })(window, document, 'script', 'dataLayer', '<?php echo $gtm; ?>');</script>
        <!-- End Google Tag Manager -->
    <?php } ?>
    <?php /* посилань на альтернативні мовні версії сайту в шапці не потрібно, вони повині бути в сайтмапі лише, але покищо залишив*/ ?>
    <?php /*foreach ($alter_lang as $lang=>$href) { ?>
    <link href="<?php echo $href; ?>" hreflang="<?php echo $lang; ?>" rel="alternate" />
<?php }*/ ?>

    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
<!--    <script>-->
<!--        $(document).ready(function () {-->
<!--            $("img").each(function (index) {-->
<!---->
<!--                $(this).attr('data-src', $(this).attr('src'));-->
<!--                $(this).attr('src','#');-->
<!--                console.log($(this).data('src'));-->
<!--            });-->
<!--            // each(function (e) {-->
<!--            //     e.-->
<!--            // })-->
<!--            // var b = $('img').attr('src');-->
<!--            // console.log(b);-->
<!--        });-->
<!--    </script>-->
<!--    <script src="catalog/view/javascript/jquery.lazyloadxt.js"></script>-->
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/icomoon/style.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/fonts/fonts.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="catalog/view/javascript/jquery/jQueryFormStyler-master/dist/jquery.formstyler.min.js"></script>
    <script src="catalog/view/javascript/social_auth.js"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEi_mKbOqvmIL0qXYZEwCZujvacMpBxGQ&callback"></script>
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
              media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
    <script src="catalog/view/javascript/common.js"></script>
    <script src="catalog/view/javascript/main.js"></script>
    <script src="catalog/view/javascript/iscroll.js"></script>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"></script>
    <?php } ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": "<?php echo $base; ?>",
            "logo": "<?php echo $logo; ?>"
        }
    </script>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if ($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<script type="application/ld+json">
    { "@context" : "http://schema.org",
        "@type" : "Organization",
        "url" : "<?php echo $site_home; ?>",
        "name" : "<?php echo $site_name; ?>",
        "logo" : "http://socks.mistery.biz.ua/image/catalog/logo.png",
        "description" : "<?php echo $description_seo;?>",
        "address": {
            "@type": "PostalAddress",
            "addressCountry": "Украина",
            "addressLocality": "",
            "postalCode": "",
            "streetAddress": "",
            "email": "<?php echo $email;?>",
            "telephone": "<?php echo $telephone;?>",
            "name": "<?php echo $site_name; ?>"
        },
        "sameAs" : [ "<?php echo $config_inst;?>"]
    }
</script>
<div class="body_inst">
    <a href="<?php echo $instagram; ?>" target="_blank"><i class="icon-instagram"></i><span>#dreamstextile</span></a>
</div>
<div class="left_bar">
    <div class="left_bar-title">
        <?php echo $text_menu; ?>
        <div class="left_bar-navigation">

            <div class="left_bar-close">
                <i class="icon-close"></i>
            </div>
        </div>
    </div>
    <div class="left_bar-content">
        <ul>
            <?php if ($informations) { ?>
                <?php $i = 1;
                foreach ($informations as $information) { ?>
                    <?php if ($i <= 1) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                        </li><?php } ?>
                    <?php $i++;
                } ?>
            <?php } ?>
            <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
            <?php if ($articles) { ?>
                <li>
                    <span><?php echo $text_customers ?><i class="icon-down-arrow"></i></span>
                    <ul class="lvl_down">
                        <?php foreach ($articles as $article) { ?>
                            <li><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="right_bar">
    <div class="right_bar-title">
        <?php echo $text_category; ?>
        <div class="right_bar-close">
            <i class="icon-close"></i>
        </div>
    </div>
    <div class="right_bar-content">
        <ul>
            <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                    <li class="dropdown">
                        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        <i class="icon-down-arrow"></i>
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                            <ul class="lvl_down">
                                <?php foreach ($children as $child) { ?>
                                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
    </div>
</div>
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 hidden-sm">
                <div class="header-left">
                    <div class="btn-left_bar hidden-lg hidden-md hidden-sm"><i class="icon-menu"></i></div>
                    <div id="logo">
                        <?php if ($logo) { ?>
                            <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                                     class="img-responsive image_desctop"/>
                                <img src="catalog/view/theme/default/image/logo_mobal.svg" title="<?php echo $name; ?>"
                                     alt="<?php echo $name; ?>"
                                     class="img-responsive image_mobal"/>

                            <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>"
                                                                    title="<?php echo $name; ?>"
                                                                    alt="<?php echo $name; ?>" class="img-responsive"/></a>
                            <?php } ?>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                    <div class="btn-right_bar hidden-lg hidden-md hidden-sm"><i class="icon-menu-grid"></i></div>
                </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-12">
                <div class="header-top">
                    <div class="header-top-logo hidden-lg hidden-md hidden-xs">
                        <?php if ($logo) { ?>
                            <?php if ($home == $og_url) { ?>
                                <img src="catalog/view/theme/default/image/logo_mobal.svg" title="<?php echo $name; ?>"
                                     alt="<?php echo $name; ?>"
                                     class="img-responsive"/>

                            <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>"
                                                                    title="<?php echo $name; ?>"
                                                                    alt="<?php echo $name; ?>" class="img-responsive"/></a>
                            <?php } ?>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                    <div class="header-top-info hidden-sm hidden-xs">
                        <?php if ($telephone) { ?><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone); ?>"
                                                     class="header-top-tel"><?php echo $telephone; ?></a><?php } ?>
                        <?php if ($email) { ?><a href="mail:<?php echo $email; ?>"><?php echo $email; ?></a><?php } ?>
                    </div>
                    <div class="header-top-navigation">
                        <?php echo $search; ?>
                        <a href="<?php if ($logged) {
                            echo $wishlist;
                        } else { ?>javascript:void(0);<?php } ?>" class="<?php if (!$logged) { ?>quick_login<?php } ?>"
                           id="wishlist-total">
                            <i class="icon-heart"></i>
                            <span><?php echo $text_wishlist; ?></span>
                        </a>
                        <?php echo $cart; ?>
                        <a href="<?php if ($logged) {
                            echo $account;
                        } else { ?>javascript:void(0);<?php } ?>" title=""
                           class="<?php if ($logged) { ?>active<?php } else { ?>quick_login<?php } ?> account"><i
                                    class="icon-user"></i></a>
                        <?php echo $language; ?>
                    </div>
                </div>
                <div id="menu" class="header-menu hidden-xs">
                    <ul class="header-menu-list">
                        <?php foreach ($categories as $category) { ?>
                            <?php if ($category['children']) { ?>
                                <li class="dropdown">
                                    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                        <ul class="header-menu-under">
                                            <?php foreach ($children as $child) { ?>
                                                <li>
                                                    <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } else { ?>
                                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                        <li class="dropdown-click">
                            <i class="icon-more"></i>
                            <ul class="header-menu-under">
                                <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                                <?php if ($informations) { ?>
                                    <?php $i = 1;
                                    foreach ($informations as $information) { ?>
                                        <?php if ($i <= 1) { ?>
                                            <li>
                                            <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                                            </li><?php } ?>
                                        <?php $i++;
                                    } ?>
                                <?php } ?>
                                <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
                                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
