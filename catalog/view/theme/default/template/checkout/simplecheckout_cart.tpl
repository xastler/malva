<div class="block_bg">
    <div class="simplecheckout-title">

        <?php echo $simplecheckout_title_2; ?>
    </div>
</div>
<div class="simplecheckout-block"
     id="simplecheckout_cart" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
    <?php if ($display_header) { ?>
        <div class="checkout-heading panel-heading"><?php echo $text_cart ?></div>
    <?php } ?>
    <?php if ($attention) { ?>
        <div class="simplecheckout-warning-block"><?php echo $attention; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
        <div class="simplecheckout-warning-block"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="checkout-lists">
        <div id="scroller">
            <div class="checkout-block">
                <?php foreach ($products as $product) { ?>
                    <div class="checkout-cart <?php if (!$product['stock'] && ($config_stock_warning || !$config_stock_checkout)) { ?> error <?php } ?>">
                        <?php if (!empty($product['recurring'])) { ?>
                            <div class="simplecheckout-recurring-product" style="border:none;"><img
                                        src="<?php echo $additional_path ?>catalog/view/theme/default/image/reorder.png" alt="" title=""
                                        style="float:left;"/>
                                <span style="float:left;line-height:18px; margin-left:10px;">
                    <strong><?php echo $text_recurring_item ?></strong>
                    <?php echo $product['profile_description'] ?>
                    </span>
                            </div>
                        <?php } ?>
                        <div class="checkout-cart-image">
                            <?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                               alt="<?php echo $product['name']; ?>"
                                                                               title="<?php echo $product['name']; ?>"/></a>
                            <?php } ?>
                        </div>
                        <div class="checkout-cart-info ">
                            <div class="name">
                                <?php echo $product['name']; ?>
                            </div>
                            <div class="options">
                                <ul>
                                    <li>Арт. <?php echo $product['sku']; ?></li>
                                    <?php foreach ($product['option'] as $option) { ?>&nbsp;
                                        <li><span><?php echo $option['name']; ?>:</span> <?php echo $option['value']; ?></li>
                                    <?php } ?>
                                    <?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                            <li>
                                                <span><?php echo $attribute['name']; ?>: </span><?php echo $attribute['text']; ?>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (!empty($product['recurring'])) { ?>
                                        <li><span><?php echo $text_payment_profile ?>:</span> <?php echo $product['profile_name'] ?>
                                        </li>
                                    <?php } ?>
                                    <?php /* if ($product['reward']) { ?>
                    <small><?php echo $product['reward']; ?></small>
                <?php } */ ?>
                                </ul>
                            </div>


                            <div class="quantity">
                                <div class="price"><?php echo $product['price']; ?></div>

                                <div class="quantity-num input-group btn-block" style="max-width: 200px;">
                    <span class="num minus">
                        <button class="" data-onclick="decreaseProductQuantity"
                                data-toggle="tooltip" type="submit">
                            <i class="icon-down-arrow"></i>
                        </button>
                    </span>
                                    <input class="form-control" type="text" data-onchange="changeProductQuantity"
                                           name="quantity[<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key']; ?>]"
                                           value="<?php echo $product['quantity']; ?>" size="1" maxlength="2"/>
                                    <span class="num plus">
                        <button class="" data-onclick="increaseProductQuantity"
                                data-toggle="tooltip" type="submit">
                            <i class="icon-down-arrow"></i>
                        </button>

                    </span>
                                </div>
                                <div class="total price"><?php echo $product['total']; ?></div>
                            </div>
                            <div class="remove">
                                <button class="" data-onclick="removeProduct"
                                        data-product-key="<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key'] ?>"
                                        data-toggle="tooltip" type="button">
                                    <i class="icon-close"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                <?php } ?>
                <?php foreach ($vouchers as $voucher_info) { ?>
                    <tr>
                        <td class="image"></td>
                        <td class="name"><?php echo $voucher_info['description']; ?></td>
                        <td class="model"></td>
                        <td class="quantity">1</td>
                        <td class="price"><?php echo $voucher_info['amount']; ?></td>
                        <td class="total"><?php echo $voucher_info['amount']; ?></td>
                        <td class="remove">
                            <i data-onclick="removeGift" data-gift-key="<?php echo $voucher_info['key']; ?>"
                               title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
                        </td>
                    </tr>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php foreach ($totals as $total) { ?>
        <div class="simplecheckout-cart-total" id="total_<?php echo $total['code']; ?>">
            <span><b><?php echo $total['title']; ?>:</b></span>
            <span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span>
            <span class="simplecheckout-cart-total-remove">
            <?php if ($total['code'] == 'coupon') { ?>
                <i data-onclick="removeCoupon" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
                <?php if ($total['code'] == 'voucher') { ?>
                    <i data-onclick="removeVoucher" title="<?php echo $button_remove; ?>"
                       class="fa fa-times-circle"></i>
                <?php } ?>
                <?php if ($total['code'] == 'reward') { ?>
                    <i data-onclick="removeReward" title="<?php echo $button_remove; ?>"
                       class="fa fa-times-circle"></i>
                <?php } ?>
        </span>
        </div>
    <?php } ?>
    <?php /*if (isset($modules['coupon'])) { ?>
            <div class="simplecheckout-cart-total">
                <span class="inputs"><?php echo $entry_coupon; ?>&nbsp;<input class="form-control" type="text"
                                                                              data-onchange="reloadAll" name="coupon"
                                                                              value="<?php echo $coupon; ?>"/></span>
            </div>
        <?php } ?>
        <?php if (isset($modules['reward']) && $points > 0) { ?>
            <div class="simplecheckout-cart-total">
                <span class="inputs"><?php echo $entry_reward; ?>&nbsp;<input class="form-control" type="text"
                                                                              name="reward" data-onchange="reloadAll"
                                                                              value="<?php echo $reward; ?>"/></span>
            </div>
        <?php } ?>
        <?php if (isset($modules['voucher'])) { ?>
            <div class="simplecheckout-cart-total">
                <span class="inputs"><?php echo $entry_voucher; ?>&nbsp;<input class="form-control" type="text"
                                                                               name="voucher" data-onchange="reloadAll"
                                                                               value="<?php echo $voucher; ?>"/></span>
            </div>
        <?php } ?>
        <?php if (isset($modules['coupon']) || (isset($modules['reward']) && $points > 0) || isset($modules['voucher'])) { ?>
            <div class="simplecheckout-cart-total simplecheckout-cart-buttons">
                <span class="inputs buttons"><a id="simplecheckout_button_cart" data-onclick="reloadAll"
                                                class="button btn-primary button_oc btn"><span><?php echo $button_update; ?></span></a></span>
            </div>
        <?php } */ ?>
    <input type="hidden" name="remove" value="" id="simplecheckout_remove">
    <div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
    <?php if ($display_weight) { ?>
        <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
    <?php } ?>
    <?php if (!$display_model) { ?>
        <style>
            .simplecheckout-cart col.model,
            .simplecheckout-cart th.model,
            .simplecheckout-cart td.model {
                display: none;
            }
        </style>
    <?php } ?>
</div>
<?php if (!$block_order) { ?>
    <div class="simplecheckout-button-block buttons" id="buttons">
        <a href="/" class="btn-max-st2 btn"><span><?php echo $button_home; ?></span></a>
        <?php if ($display_agreement_checkbox) { ?><span id="agreement_checkbox"><label><input type="checkbox" name="agreement" value="1" <?php if ($agreement == 1) { ?>checked="checked"<?php } ?> /><?php echo $text_agreement; ?></label>&nbsp;</span><?php } ?>
        <?php if ($steps_count > 1) { ?>
            <a class="button  button_oc btn" data-onclick="nextStep" id="simplecheckout_button_next"><span><?php echo $button_next; ?></span></a>
        <?php } ?>
        <a class="btn-max-default btn" data-onclick="createOrder" id="button-confirm"><span><?php echo $button_order; ?></span></a>

        <?php if ($display_back_button) { ?>
            <a class="button  button_oc btn" data-onclick="backHistory" id="simplecheckout_button_back"><span><?php echo $button_back; ?></span></a>
        <?php } ?>
        <?php if ($steps_count > 1) { ?>
            <a class="button  button_oc btn" data-onclick="previousStep" id="simplecheckout_button_prev"><span><?php echo $button_prev; ?></span></a>
        <?php } ?>

    </div>
<?php } ?>

<script>
    var myScroll;
    function scroll() {
        myScroll = new IScroll('.checkout-lists', {
            bounce: false,
            scrollbars: true,
            mouseWheel: true,
            interactiveScrollbars: true,
            mouseWheelSpeed: 120
        });
    }
    if($('.checkout-lists').height()>=886){
        scroll();
    }
</script>
