<?php
// Heading
$_['heading_title']  = 'Контакты';

// Text
$_['text_location']   = 'Наша Адрес ';
$_['text_store']      = 'Наши магазины ';
$_['text_contact']    = 'напишите нам ';
$_['text_address']    = 'Адрес ';
$_['text_telephone1'] = 'Отдел продаж: ';
$_['text_telephone2'] = 'Viber:';
$_['text_telephone3'] = 'Telegram:';
$_['text_fax']        = 'Факс ';
$_['text_open']       = 'Время работы ';
$_['text_comment']    = 'Комментарий ';
$_['text_success']    = 'Ваш запрос был успешно отправлен администрации магазина!';
$_['message_subject']   = 'Сообщение';

// Entry
$_['entry_name']     = 'Ваше имя ';
$_['entry_email']    = 'E-Mail ';
$_['entry_phone']    = 'Телефон ';
$_['entry_enquiry']  = 'Сообщение ';

// Email
$_['email_subject']  = 'Сообщение ';
$_['name_subject']   = 'Имя ';
$_['tell_subject']   = 'Телефон ';
$_['email_subject']  = 'E-Mail ';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов! ';
$_['error_email']    = 'E-Mail указано некорректно! ';
$_['error_enquiry']  = 'Сообщение должно быть от 10 до 3000 символов! ';
