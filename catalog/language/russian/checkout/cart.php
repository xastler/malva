<?php
// Heading
$_['heading_title']            = 'Корзина заказов';

// Text
$_['text_success']             = '<div class="popups_block-name">%s добавлен в корзину</div><a href="%s">оформить заказ</a>';
$_['text_remove']              = 'Корзина успешно обновлен!';
$_['text_login']               = 'вам необходимо <a href="%s">авторизоваться</a> или <a href="%s">создать аккаунт</a> для просмотра цен!';
$_['text_items']               = '%s';
$_['text_points']              = 'Призовые баллы: %s';
$_['text_next']                = 'Воспользуйтесь дополнительными возможностями';
$_['text_next_choice']         = 'Если у вас есть код купона на скидку или бонусные баллы, которые вы хотите использовать, выберите соответствующий пункт. <br> Также, можно (приблизительно) узнать стоимость доставки в ваш регион.';
$_['text_empty']               = 'В корзине пусто ';
$_['text_day']                 = 'день ';
$_['text_week']                = 'неделю ';
$_['text_semi_month']          = 'полмесяца ';
$_['text_month']               = 'месяц ';
$_['text_year']                = 'год ';
$_['text_trial']               = '%s каждый %s %s для %s оплаты ';
$_['text_recurring']           = '%s каждый %s %s ';
$_['text_length']              = '  для %s оплат ';
$_['text_until_cancelled']     = 'к отмене ';
$_['text_recurring_item']      = 'Повторяющиеся ';
$_['text_payment_recurring']   = 'Профиль платежа ';
$_['text_trial_description']   = '%s каждый %d %s (s) для %d платежа(ов) тогда ';
$_['text_payment_description'] = '%s каждый %d %s (s) для %d платежа(ов) ';
$_['text_payment_cancel']      = '%s каждый %d %s (s) пока не будет отменено ';

// Column
$_['column_image']           = 'Изображение ';
$_['column_name']            = 'Наименование товара ';
$_['column_model']           = 'Модель ';
$_['column_quantity']        = 'Количество ';
$_['column_price']           = 'Цена за шт. ';
$_['column_total']           = 'Всего ';

// Error
$_['error_stock']              = 'Продукты отмеченные *** отсутствуют в необходимом количестве или их нет в наличии! ';
$_['error_minimum']            = 'Минимальное количество товара для заказа %s равна %s ';
$_['error_required']           = '%s необходим! ';
$_['error_product']            = 'В вашей корзине нет товаров! ';
$_['error_recurring_required'] = 'Выберите платежный профиль! ';