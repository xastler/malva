<?php
// Text
$_['text_refine']       = 'Subcategories';
$_['heading_title']     = 'Headlines';
$_['text_error']        = 'Articles category not found!';
$_['text_empty']        = 'There are no articles here yet. Come back here soon, we will add some.';
$_['text_comments']     = 'comments on this article';
$_['text_comments_v']   = 'view comments';
$_['button_more']       = 'Подробнее';
$_['text_posted_by']    = 'Posted by';
$_['text_posted_on']    = 'On';
$_['text_posted_pon']   = '';
$_['text_posted_in']    = 'Added in';
$_['text_updated_on']   = 'Updated on';
$_['go_to_headlines']   = 'Go to Headlines';
$_['mon01']             = 'Январь ';
$_['mon02']             = 'Февраль ';
$_['mon03']             = 'Март ';
$_['mon04']             = 'Апрель ';
$_['mon05']             = 'Май ';
$_['mon06']             = 'Июнь ';
$_['mon07']             = 'Июль ';
$_['mon08']             = 'Август ';
$_['mon09']             = 'Сентябрь ';
$_['mon10']             = 'Октябрь ';
$_['mon11']             = 'Ноябрь ';
$_['mon12']             = 'Декабрь ';
$_['text_photo']     = 'фото';
?>