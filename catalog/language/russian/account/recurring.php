<?php
$_['heading_title'] 		    = 'Регулярные платежи ';
$_['button_continue'] 		    = 'Продолжить';
$_['button_view'] 		        = 'Просмотр ';
$_['text_empty'] 		        = 'У вас нет ни одного профиля с регулярными платежами ';
$_['text_product'] 		        = 'Товар: ';
$_['text_order'] 		        = 'Заказ: ';
$_['text_quantity'] 		    = 'Количество: ';
$_['text_account'] 		        = 'Аккаунт ';
$_['text_action'] 		        = 'Действие ';
$_['text_recurring'] 		    = 'Регулярные платежи ';
$_['text_transactions'] 	    = 'Переводы ';
$_['button_return'] 		    = 'Возвращение ';
$_['text_empty_transactions'] 	= 'Есть переводов для этого профиля ';

$_['column_date_added'] 	= 'Создан ';
$_['column_type'] 		    = 'Тип ';
$_['column_amount'] 		= 'Сумма ';
$_['column_status'] 		= 'Статус ';
$_['column_product'] 		= 'Товар ';
$_['column_action'] 		= 'Действие ';
$_['column_recurring_id'] 	= 'Профиль № ';

$_['text_recurring_detail'] 	 = 'Детали регулярного платежа ';
$_['text_recurring_id'] 	     = 'ID Профиля: ';
$_['text_payment_method'] 	     = 'Метод оплаты: ';
$_['text_date_added'] 		     = 'Создан: ';
$_['text_recurring_description'] = 'Описание: ';
$_['text_status'] 		         = 'Статус: ';
$_['text_ref'] 			         = 'Сноска: ';

$_['text_status_active'] 	= 'Активный ';
$_['text_status_inactive'] 	= 'Не активный ';
$_['text_status_cancelled'] = 'Отменено ';
$_['text_status_suspended'] = 'Замороженное ';
$_['text_status_expired'] 	= 'Закончился ';
$_['text_status_pending'] 	= 'Ждет ';

$_['text_transaction_date_added'] 		    = 'Создан ';
$_['text_transaction_payment'] 			    = 'Платеж ';
$_['text_transaction_outstanding_payment'] 	= 'НЕ поступивший платеж ';
$_['text_transaction_skipped'] 			    = 'Платеж пропущен ';
$_['text_transaction_failed'] 			    = 'Проблема с оплатой ';
$_['text_transaction_cancelled'] 		    = 'Отменено ';
$_['text_transaction_suspended'] 		    = 'Замороженное ';
$_['text_transaction_suspended_failed'] 	= 'Заморожен из-за неудачный платеж ';
$_['text_transaction_outstanding_failed'] 	= 'Платеж не прошел ';
$_['text_transaction_expired'] 			    = 'Закончился ';

$_['error_not_cancelled'] 		= 'Ошибка: %s ';
$_['error_not_found'] 			= 'Нельзя отключить профиль ';
$_['text_cancelled'] 			= 'Регулярный платеж был закрыт ';