<?php
// Heading 
$_['heading_title']      = 'Бонусные баллы';

// Column
$_['column_date_added']  = 'Дата начисления ';
$_['column_description'] = 'Начислено за: ';
$_['column_points']      = 'Всего баллов ';

// Text
$_['text_account']       = 'Личный Кабинет ';
$_['text_reward']        = 'Бонусные баллы ';
$_['text_total']         = 'Накоплен Бонусных баллов: ';
$_['text_empty']         = 'У вас нет Бонусных баллов! ';