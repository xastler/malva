<?php
// Heading 
$_['heading_title']        = 'Регистрация';

// Text
$_['text_account']         = 'Личный Кабинет ';
$_['text_register']        = 'Регистрация ';
$_['text_account_already'] = 'Если Вы уже зарегистрированы, перейдите на страницу <a href="%s"> авторизации </a>.';
$_['text_your_details']    = 'Основные данные ';
$_['text_your_address']    = 'Ваш адрес ';
$_['text_newsletter']      = 'Подписка на новости ';
$_['text_your_password']   = 'Ваш пароль';
$_['text_agree']           = 'Мною прочитаны и я соглашаюсь документу <a href="%s" class="agree"> <b>%s </b> </a>';

// Entry
$_['entry_customer_group'] = 'Группа покупателей ';
$_['entry_firstname']      = 'Имя ';
$_['entry_lastname']       = 'Фамилия ';
$_['entry_email']          = 'E-Mail ';
$_['entry_telephone']      = 'Телефон ';
$_['entry_fax']            = 'Факс ';
$_['entry_company']        = 'Компания ';
$_['entry_address_1']      = 'Адрес ';
$_['entry_address_2']      = 'Адрес (дополнительно) ';
$_['entry_postcode']       = 'Индекс ';
$_['entry_city']           = 'Город ';
$_['entry_country']        = 'Страна ';
$_['entry_zone']           = 'Регион / Область ';
$_['entry_newsletter']     = 'Подписаться ';
$_['entry_password']       = 'Пароль ';
$_['entry_confirm']        = 'Подтвердить ';

// Error
$_['error_exists']         = 'Этот E-Mail уже зарегистрирован! ';
$_['error_firstname']      = 'Имя может содержать от 1 до 32 символов! ';
$_['error_lastname']       = 'Фамилия должна содержать от 1 до 32 символов! ';
$_['error_email']          = 'E-Mail введен неверно! ';
$_['error_telephone']      = 'В номера телефона должен быть от 3 до 32 цифр! ';
$_['error_address_1']      = 'Адрес должен содержать от 3 до 128 символов! ';
$_['error_city']           = 'Название города должен содержать от 2 до 128 символов! ';
$_['error_postcode']       = 'В индексе должно быть от 2 до 10 символов! ';
$_['error_country']        = 'Выберите страну! ';
$_['error_zone']           = 'Выберите регион / область! ';
$_['error_custom_field']   = '% S необходим! ';
$_['error_password']       = 'В пароле должно быть от 4 до 20 символов! ';
$_['error_confirm']        = 'Пароли не совпадают! ';
$_['error_agree']          = 'Для регистрации Вы должны согласиться с документом% s ';