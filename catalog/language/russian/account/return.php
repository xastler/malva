<?php
// Heading 
$_['heading_title']      = 'Возврат товара';

// Text
$_['text_account']       = 'Личный Кабинет ';
$_['text_return']        = 'Информация о Возвращение ';
$_['text_return_detail'] = 'Основные данные ';
$_['text_description']   = 'Пожалуйста, заполните форму на Возврат товара. ';
$_['text_order']         = 'Информация о заказе ';
$_['text_product']       = 'Информация о товаре и причина Возвращение ';
$_['text_message']       = '<p> Вы отправили заявку на Возврат товара </p> <p> Сообщение о статусе заявки будут приходить на ваш e-mail. Спасибо! </p>';
$_['text_return_id']     = '№ Возвращение ';
$_['text_order_id']      = '№ заказа ';
$_['text_date_ordered']  = 'Дата заказа ';
$_['text_status']        = 'Статус ';
$_['text_date_added']    = 'Дата заявки ';
$_['text_comment']       = 'Комментарий ';
$_['text_history']       = 'История Возвращение ';
$_['text_empty']         = 'У Вас не было раньше Возврат товаров! ';
$_['text_agree']         = 'Мною прочитаны и я соглашаюсь с документом <a href="%s" class="agree"> <b>%s </b> </a>';

// Column
$_['column_return_id']   = '№ Возвращение: ';
$_['column_order_id']    = '№ заказа: ';
$_['column_status']      = 'Статус ';
$_['column_date_added']  = 'Дата заявки: ';
$_['column_customer']    = 'Покупатель: ';
$_['column_product']     = 'Наименование товара: ';
$_['column_model']       = 'Модель: ';
$_['column_quantity']    = 'Количество: ';
$_['column_price']       = 'Цена ';
$_['column_opened']      = 'Заявка открыта: ';
$_['column_comment']     = 'Комментарий ';
$_['column_reason']      = 'Причина Возвращение ';
$_['column_action']      = 'Действие ';

// Entry
$_['entry_order_id']     = '№ заказа: ';
$_['entry_date_ordered'] = 'Дата заказа: ';
$_['entry_firstname']    = 'Имя: ';
$_['entry_lastname']     = 'Фамилия: ';
$_['entry_email']        = 'E-Mail: ';
$_['entry_telephone']    = 'Телефон: ';
$_['entry_product']      = 'Наименование: ';
$_['entry_model']        = 'Модель: ';
$_['entry_quantity']     = 'Количество: ';
$_['entry_reason']       = 'Причина: ';
$_['entry_opened']       = 'Распаковано: ';
$_['entry_fault_detail'] = 'Описание: ';

// Error
$_['text_error']         = 'По вашему запросу ничего не найдено! ';
$_['error_order_id']     = 'Не указан № заказа! ';
$_['error_firstname']    = 'Имя должно быть от 1 до 32 символов! ';
$_['error_lastname']     = 'Фамилия должна быть от 1 до 32 символов! ';
$_['error_email']        = 'E-Mail адрес введен неверно! ';
$_['error_telephone']    = 'Номер телефона должен быть от 3 до 32 символов! ';
$_['error_product']      = 'Наименование товара должно быть от 3 до 255 символов! ';
$_['error_model']        = 'Модель должна быть от 3 до 64 символов! ';
$_['error_reason']       = 'Необходимо указать причину Возврат товара! ';
$_['error_agree']        = 'Для оформления возврата Вы должны согласиться с документом с %s!';