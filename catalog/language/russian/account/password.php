<?php
// Heading 
$_['heading_title']  = 'Изменение пароля';

// Text
$_['text_account']   = 'Личный Кабинет ';
$_['text_password']  = 'Ваш пароль';
$_['text_success']   = 'Пароль успешно изменен! ';
$_['button_save']   = 'Сохранить';

// Entry
$_['entry_password']      = 'Новый пароль';
$_['entry_confirm']       = 'Повторите новый пароль';
$_['entry_old_password']  = 'Текущий пароль';

// Error
$_['error_password'] = 'Пароль должен быть от 4 до 20 символов! ';
$_['error_old_password'] = 'Неправильный пароль!';
$_['error_confirm']  = 'Пароли не совпадают! ';