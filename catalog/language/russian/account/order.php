<?php
// Heading 
$_['heading_title']         = 'История заказов';

// Text
$_['text_account']          = 'Личный Кабинет ';
$_['text_order']            = 'Заказ ';
$_['text_order_detail']     = 'Детали заказа ';
$_['text_invoice_no']       = '№ Счета: ';
$_['text_order_id']         = '№ Заказ: ';
$_['text_date_added']       = 'Добавлено: ';
$_['text_shipping_address'] = 'Адрес доставки ';
$_['text_shipping_method']  = 'Способ доставки: ';
$_['text_payment_address']  = 'Платежная адрес ';
$_['text_payment_method']   = 'Способ оплаты: ';
$_['text_comment']          = 'Комментарий к заказу ';
$_['text_history']          = 'История заказов ';
$_['text_success']          = 'Товар по заказу <a href="%s">% s </a> добавлен в <a href="%s"> вашу корзину </a>!';
$_['text_empty']            = 'Вы еще не осуществляли покупок! ';
$_['text_error']            = 'Запрашиваемое заказ не найдено! ';
$_['text_num_table']            = '№ Заказ';

// Column
$_['column_order_id']       = '№ Заказ ';
$_['column_product']        = 'Количество товара ';
$_['column_customer']       = 'Покупатель ';
$_['column_name']           = 'Название товара';
$_['column_model']          = 'Модель ';
$_['column_quantity']       = 'Количество ';
$_['column_price']          = 'Цена ';
$_['column_total']          = 'Всего';
$_['column_action']         = 'Действие ';
$_['column_date_added']     = 'Добавлено ';
$_['column_status']         = 'Статус';
$_['column_comment']        = 'Комментарий ';

// Error
$_['error_reorder']         = '%s в данный момент недоступен для заказа.';