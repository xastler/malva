<?php
// Heading 
$_['heading_title']  = 'Изменение пароля';

// Text
$_['text_account']   = 'Кабинет Партнера ';
$_['text_password']  = 'Ваш пароль';
$_['text_success']   = 'Пароль успешно изменен! ';

// Entry
$_['entry_password'] = 'Пароль ';
$_['entry_confirm']  = 'Подтвердить ';

// Error
$_['error_password'] = 'Пароль должен быть от 4 до 20 символов! ';
$_['error_confirm']  = 'Пароли не совпадают! ';