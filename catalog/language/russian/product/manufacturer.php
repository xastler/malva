<?php
// Heading
$_['heading_title']     = 'Перечень производителей';

// Text
$_['text_brand']        = 'Бренд ';
$_['text_index']        = 'Алфавитный указатель: ';
$_['text_error']        = 'Изготовителя не найдено! ';
$_['text_empty']        = 'Нет ни одного товара. ';
$_['text_quantity']     = 'Количество: ';
$_['text_manufacturer'] = 'Производитель: ';
$_['text_model']        = 'Модель: ';
$_['text_points']       = 'Бонусные баллы: ';
$_['text_price']        = 'Цена: ';
$_['text_tax']          = 'Без налога: ';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать:';
$_['text_default']      = 'По умолчанию ';
$_['text_name_asc']     = 'По Именем (A - Я) ';
$_['text_name_desc']    = 'По Именем (Я - A) ';
$_['text_price_asc']    = 'По Ценой (рост) ';
$_['text_price_desc']   = 'По Ценой (уменьшение) ';
$_['text_rating_asc']   = 'По Рейтингу (рост) ';
$_['text_rating_desc']  = 'По Рейтингу (уменьшение) ';
$_['text_model_asc']    = 'По Модели (А - Я) ';
$_['text_model_desc']   = 'По Модели (Я - А) ';
$_['text_limit']        = 'На странице: ';