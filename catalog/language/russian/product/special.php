<?php
// Heading
$_['heading_title']     = 'Товары со скидкой';

// Text
$_['text_empty']        = 'В данный момент товары со скидкой отсутствуют ';
$_['text_quantity']     = 'Количество: ';
$_['text_manufacturer'] = 'Производитель: ';
$_['text_model']        = 'Модель: ';
$_['text_points']       = 'Бонусные баллы: ';
$_['text_price']        = 'Цена: ';
$_['text_tax']          = 'Без НДС: ';
$_['text_compare']      = 'Сравнение товаров (%s) ';
$_['text_sort']         = 'Сортировать:';
$_['text_sort_news']    = 'сначала новинки ';
$_['text_sort_special'] = 'сначала акционные ';
$_['text_default']      = 'по умолчанию ';
$_['text_name_asc']     = 'По Именем (A - Я) ';
$_['text_name_desc']    = 'По Именем (Я - A) ';
$_['text_price_asc']    = 'цена по росту ';
$_['text_price_desc']   = 'цена по убыванию ';
$_['text_rating_asc']   = 'По Рейтингу (рост) ';
$_['text_rating_desc']  = 'По Рейтингу (уменьшение) ';
$_['text_model_asc']    = 'По Модели (A - Я) ';
$_['text_model_desc']   = 'По Модели (Я - A) ';
$_['text_limit']        = 'На странице: ';
$_['text_ajax_pp']      = 'загрузить еще ';
$_['text_special']      = 'Sale ';
$_['text_filter_btn']      = 'ФИЛЬТР';