<?php
// Text
$_['text_search']                             = 'Поиск';
$_['text_brand']                              = 'Бренд ';
$_['text_manufacturer']                       = 'Производитель: ';
$_['text_model']                              = 'Модель: ';
$_['text_reward']                             = 'Бонусные баллы: ';
$_['text_points']                             = 'Цена в бонусных баллах: ';
$_['text_stock']                              = 'Наличие:';
$_['text_instock']                            = 'На складе ';
$_['text_tax']                                = 'Без НДС: ';
$_['text_discount']                           = 'или более ';
$_['text_option']                             = 'Доступные опции ';
$_['text_minimum']                            = 'Минимальное количество для заказа этого товара: %s.';
$_['text_reviews']                            = '%s отзывов';
$_['text_write']                              = 'Написать отзыв';
$_['text_login']                              = 'Пожалуста  авторизуйтесь или зарегистрируйтесь для просмотра';
$_['text_no_reviews']                         = 'Нет отзывов об этом товаре.';
$_['text_note']                               = '<span class = "text-danger"> Внимание: </span> HTML не поддерживает!';
$_['text_success']                            = 'Спасибо за ваш отзыв. Он был направлен на модерацию. ';
$_['text_related']                            = 'Сопутствующие товары ';
$_['text_tags']                               = 'Теги: ';
$_['text_error']                              = 'Товар не найден! ';
$_['text_payment_recurring']                  = 'Платежные профили ';
$_['text_trial_description']                  = '%s каждый %d %s и для %d оплат(ы),';
$_['text_payment_description']                = '%s каждый %d %s (-и) с %d платежей (в)';
$_['text_payment_cancel']                     = '%s каждый %d %s(s) до отмены';
$_['text_day']                                = 'день ';
$_['text_week']                               = 'неделю ';
$_['text_semi_month']                         = 'полумесяца ';
$_['text_month']                              = 'месяц ';
$_['text_year']                               = 'год ';
$_['text_wishlist_btn']                       = 'в список желаний';
$_['text_art']                           = 'артикул';
$_['text_photo']                           = 'фото';

// Entry
$_['entry_qty']                               = 'Количество ';
$_['entry_name']                              = 'Ваше имя: ';
$_['entry_review']                            = 'Ваш отзыв ';
$_['entry_rating']                            = 'Рейтинг ';
$_['entry_good']                              = 'Хорошо ';
$_['entry_bad']                               = 'Плохо ';

// Tabs
$_['tab_description']                         = 'детали товара ';
$_['tab_attribute']                           = 'Характеристики ';
$_['tab_info']                                = 'Доставка и оплата ';
$_['tab_review']                              = 'Отзывы (%s) ';
$_['tab_storage']                             = 'Состав и уход ';
$_['tab_recommendations']                     = 'Рекомендации ';

// Error
$_['error_name']                              = 'Имя должно быть от 3 до 25 символов! ';
$_['error_text']                              = 'Текст отзыва должен быть от 25 до 1000 символов! ';
$_['error_rating']                            = 'Пожалуйста, поставьте оценку! ';