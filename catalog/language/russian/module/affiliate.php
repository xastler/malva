<?php
// Heading
$_['heading_title']    = 'Кабинет Партнера';

// Text
$_['text_register']    = 'Авторизация ';
$_['text_login']       = 'Регистрация ';
$_['text_logout']      = 'Войти ';
$_['text_forgotten']   = 'Напомнить пароль ';
$_['text_account']     = 'Личный кабинет ';
$_['text_edit']        = 'Данные учетной записи ';
$_['text_password']    = 'Изменение пароля';
$_['text_payment']     = 'Способ оплаты ';
$_['text_tracking']    = 'Направления Партнера ';
$_['text_transaction'] = 'История выплат ';
