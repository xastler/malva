<?php
// Heading
$_['heading_title']    = 'Личный Кабинет';

// Text
$_['text_register']    = 'Регистрация ';
$_['text_login']       = 'Авторизация ';
$_['text_logout']      = 'Выход ';
$_['text_forgotten']   = 'Напомнить пароль ';
$_['text_account']     = 'Личный кабинет ';
$_['text_edit']        = 'Личные данные';
$_['text_password']    = 'Изменение пароля';
$_['text_address']     = 'Адреса доставки ';
$_['text_wishlist']    = 'Мои закладки ';
$_['text_order']       = 'Заказ';
$_['text_download']    = 'Файлы для скачивания ';
$_['text_reward']      = 'Бонусные баллы ';
$_['text_return']      = 'Возврат товара ';
$_['text_transaction'] = 'История платежей ';
$_['text_newsletter']  = 'Подписка на новости ';
$_['text_recurring']   = 'Регулярные платежи ';