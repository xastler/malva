<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = '%s';
$_['text_shopping_cart'] = 'Корзина заказов ';
$_['text_category']      = 'Категории ';
$_['text_account']       = 'Личный кабинет ';
$_['text_register']      = 'Реестрацая ';
$_['text_login']         = 'Авторизация ';
$_['text_order']         = 'История заказов ';
$_['text_transaction']   = 'История платежей ';
$_['text_download']      = 'Файлы для скачивания ';
$_['text_logout']        = 'Выход ';
$_['text_checkout']      = 'оформление заказа';
$_['text_search']        = 'Поиск ';
$_['text_all']           = 'Показать все ';
$_['text_page']          = 'Страница ';
$_['text_news']          = 'Блог ';
$_['text_menu']          = 'Меню ';
$_['text_customers']     = 'клиенты ';
