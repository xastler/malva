<?php
// Text
//$_['text_information']  = 'Інформація';
$_['text_information']  = 'Мальва ';
$_['text_service']      = 'Служба поддержки ';
//$_['text_extra']       = 'Додатково';
$_['text_extra']        = 'клиентам ';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара ';
$_['text_sitemap']      = 'Карта сайта ';
$_['text_manufacturer'] = 'Производители ';
$_['text_voucher']      = 'Подарочные сертификаты ';
$_['text_affiliate']    = 'Партнеры ';
$_['text_special']      = 'Sale ';
$_['text_account']      = 'Личный кабинет ';
$_['text_order']        = 'История заказов ';
$_['text_wishlist']     = 'Мои закладки ';
$_['text_newsletter']   = 'Рассылка новостей ';
$_['text_powered']      = '&copy; Copyright. %s %s. Все права защищены ';

$_['entry_name']      	= 'Ваше имя ';
$_['entry_phone']       = 'Телефон ';
$_['text_call']      	= 'Обратный звонок ';
$_['text_send']      	= 'Отправить ';
$_['text_loading']      = 'Обработка ';
$_['text_news']         = 'Блог ';
$_['text_catalog']      = 'Продукция ';