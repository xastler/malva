<?php
// Text
$_['text_signin_register']    = 'Вход / Регистрация ';
$_['text_login']   			  = 'Войти в систему';
$_['text_register']    		  = 'Регистр';

$_['text_new_customer']       = 'Новый покупатель';
$_['text_returning']          = 'Авторизация';
$_['text_returning_customer'] = 'Я постоянный клиент ';
$_['text_details']            = 'Ваши персональные данные';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Ваше имя ';
$_['entry_password']          = 'Пароль ';
$_['entry_surname']           = 'Фамилия ';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_open_pass']          = 'Показать пароль ';
$_['text_login_social']       = 'Вход через соцсети ';
$_['text_agree']              = 'я согласен <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Вход';
$_['button_register']         = 'зарегистрироваться ';

//Error
$_['error_name']           = 'Имя может содержать от 1 до 32 символов! ';
$_['error_email']          = 'Адрес электронной почты не действительны! ';
$_['error_telephone']      = 'Телефон должен быть от 3 до 32 символов!';
$_['error_password']       = 'Пароль должен содержать от 4 до 20 символов! ';
$_['error_surname']        = 'Фамилия должна содержать от 1 до 32 символов! ';
$_['error_exists']         = 'Адрес электронной почты не действительны! ';
$_['error_agree']          = 'Вы должны согласиться на% s! ';
$_['error_warning']        = 'Пожалуйста, внимательно проверьте форму на наличие ошибок! ';
$_['error_approved']       = 'Ваша учетная запись требует подтверждения, прежде чем вы сможете войти. ';
$_['error_login']          = 'Неправильный E-mail / пароль';