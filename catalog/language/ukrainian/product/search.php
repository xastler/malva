<?php
// Heading
$_['heading_title']     = 'Пошук';
$_['heading_tag']	= 'Тег - ';

// Text
$_['text_search']       = 'Товари, що відповідають критеріям пошуку';
$_['text_keyword']      = 'Ключові слова';
$_['text_category']     = 'Усі категорії';
$_['text_sub_category'] = 'Шукати в підкатегоріях';
$_['text_empty']        = 'Немає товарів, які відповідають критеріям пошуку.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без ПДВ:';
$_['text_reviews']      = 'На підставі %s відгуків.';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_sort']         = 'Сортувати:';
$_['text_sort_news']    = 'спочатку новинки';
$_['text_sort_special'] = 'спочатку акційні';
$_['text_default']      = 'за умовчанням';
$_['text_name_asc']     = 'За Ім’ям (A - Я)';
$_['text_name_desc']    = 'За Ім’ям (Я - A)';
$_['text_price_asc']    = 'ціна по зростанню';
$_['text_price_desc']   = 'ціна по спаданню';
$_['text_rating_asc']   = 'За Рейтингом (зростання)';
$_['text_rating_desc']  = 'За Рейтингом (зменшення)';
$_['text_model_asc']    = 'За Моделлю (A - Я)';
$_['text_model_desc']   = 'За Моделлю (Я - A)';
$_['text_limit']        = 'На сторінці:';
$_['text_ajax_pp']      = 'загрузить еще';
$_['text_special']      = 'Sale';
$_['text_filter_btn']      = 'ФІЛЬТР';

// Entry
$_['entry_search']      = 'Критерії пошуку';
$_['entry_description'] = 'Шукати в описі товару';