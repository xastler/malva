<?php
// Heading
$_['heading_title']     = 'Товари зі знижкою';

// Text
$_['text_empty']        = 'У даний момент товари зі знижкою відсутні';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без ПДВ:';
$_['text_compare']      = 'Порівняння товарів (%s)';
$_['text_sort']         = 'Сортувати:';
$_['text_sort_news']    = 'спочатку новинки';
$_['text_sort_special'] = 'спочатку акційні';
$_['text_default']      = 'за умовчанням';
$_['text_name_asc']     = 'За Ім’ям (A - Я)';
$_['text_name_desc']    = 'За Ім’ям (Я - A)';
$_['text_price_asc']    = 'ціна по зростанню';
$_['text_price_desc']   = 'ціна по спаданню';
$_['text_rating_asc']   = 'За Рейтингом (зростання)';
$_['text_rating_desc']  = 'За Рейтингом (зменшення)';
$_['text_model_asc']    = 'За Моделлю (A - Я)';
$_['text_model_desc']   = 'За Моделлю (Я - A)';
$_['text_limit']        = 'На сторінці:';
$_['text_ajax_pp']      = 'загрузить еще';
$_['text_special']      = 'Sale';
$_['text_filter_btn']      = 'ФІЛЬТР';