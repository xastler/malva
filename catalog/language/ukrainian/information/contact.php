<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наша Адреса';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'напишіть нам';
$_['text_address']   = 'Адреса';
$_['text_telephone1'] = 'Відділ продаж:';
$_['text_telephone2'] = 'Viber:';
$_['text_telephone3'] = 'Telegram:';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментар';
$_['text_success']   = 'Ваш запит був успішно відправлений адміністрації магазину!';
$_['message_subject']   = 'Повідомлення';

// Entry
$_['entry_name']     = 'Ваше ім’я';
$_['entry_email']    = 'E-Mail';
$_['entry_phone']    = 'Телефон';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення';
$_['name_subject']  = 'Ім’я';
$_['tell_subject']  = 'Телефон';
$_['email_subject']  = 'E-Mail';

// Errors
$_['error_name']     = 'Ім’я має бути від 3 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';
