<?php
// Heading 
$_['heading_title']  = 'Зміна пароля';

// Text
$_['text_account']   = 'Особистий Кабінет';
$_['text_password']  = 'Ваш пароль';
$_['text_success']   = 'Ваш пароль успішно змінений!';
$_['button_save']   = 'Зберегти';

// Entry
$_['entry_password']      = 'Новий пароль';
$_['entry_confirm']       = 'Повторіть новий пароль';
$_['entry_old_password']  = 'Поточний пароль';

// Error
$_['error_password'] = 'Пароль має бути від 4 до 20 символів!';
$_['error_old_password'] = 'Неправильний пароль!';
$_['error_confirm']  = 'Паролі не співпадають!';