<?php
// Heading
$_['heading_title'] = '404';
$_['heading_title_txt'] = 'помилка <span>404</span>';

// Text
$_['text_error']    = 'Вибачте, але сторінку яку ви шукаєте, не знайдено';
$_['button_home']    = 'на головну';