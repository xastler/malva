<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';
$_['text_login']   			  = 'Sign In';
$_['text_register']    		  = 'Register';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Авторизація';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Ваше ім`я';
$_['entry_password']          = 'Пароль';
$_['entry_surname']           = 'Прізвище';
$_['entry_telephone']         = 'Telephone';
$_['text_forgotten']          = 'Забули пароль?';
$_['text_open_pass']          = 'Показати пароль';
$_['text_login_social']       = 'Вхід через соцмережі';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'вхід';
$_['button_register']            = 'зареєструватись';

//Error
$_['error_name']           = 'І`мя має містити від 1 до 32 символів!';
$_['error_email']          = 'Адреса електронної пошти не дійсні!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_password']       = 'Пароль має містити від 4 до 20 символів!';
$_['error_surname']       = 'Прізвище має містити від 1 до 32 символів!';
$_['error_exists']         = 'Адреса електронної пошти не дійсні!';
$_['error_agree']          = 'You must agree to the %s!';
$_['error_warning']        = 'Please check the form carefully for errors!';
$_['error_approved']       = 'Your account requires approval before you can login.';
$_['error_login']          = 'Неправильний E-mail / пароль';