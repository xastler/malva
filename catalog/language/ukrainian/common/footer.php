<?php
// Text
//$_['text_information']  = 'Інформація';
$_['text_information']  = 'Мальва';
$_['text_service']      = 'Служба підтримки';
//$_['text_extra']        = 'Додатково';
$_['text_extra']        = 'клієнтам';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення товару';
$_['text_sitemap']      = 'Мапа сайту';
$_['text_manufacturer'] = 'Виробники';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Sale';
$_['text_account']      = 'Особистий кабінет';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Мої закладки';
$_['text_newsletter']   = 'Розсилка новин';
$_['text_powered']      = '&copy; Copyright. %s %s. Всі права захищено';

$_['entry_name']      	= 'Ваше ім`я';
$_['entry_phone']       = 'Телефон';
$_['text_call']      	= 'Зворотній дзвінок';
$_['text_send']      	= 'Відправити';
$_['text_loading']      = 'Опрацювання';
$_['text_news']         = 'Блог';
$_['text_catalog']      = 'Продукція';