<?php
// Text
$_['text_refine']       = 'Subcategories';
$_['heading_title']     = 'Headlines';
$_['text_error']        = 'Articles category not found!';
$_['text_empty']        = 'There are no articles here yet. Come back here soon, we will add some.';
$_['text_comments']     = 'comments on this article';
$_['text_comments_v']   = 'view comments';
$_['button_more']       = 'Детальніше';
$_['text_posted_by']    = 'Posted by';
$_['text_posted_on']    = 'On';
$_['text_posted_pon']   = '';
$_['text_posted_in']    = 'Added in';
$_['text_updated_on']   = 'Updated on';
$_['go_to_headlines']   = 'Go to Headlines';
$_['mon01']          = 'Січень';
$_['mon02']          = 'Лютий ';
$_['mon03']          = 'Березень';
$_['mon04']          = 'Квітень';
$_['mon05']          = 'Травень';
$_['mon06']          = 'Червень';
$_['mon07']          = 'Липень';
$_['mon08']          = 'Серпень';
$_['mon09']          = 'Вересень';
$_['mon10']          = 'Жовтень';
$_['mon11']          = 'Листопад';
$_['mon12']          = 'Грудень';
$_['text_photo']     = 'фото';
?>