<?php
class ModelShippingFlat198403 extends Model {
	function getQuote($address) {
		$this->load->language('shipping/flat198403');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('flat198403_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('flat198403_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['flat198403'] = array(
				'code'         => 'flat198403.flat198403',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('flat198403_cost'),
				'tax_class_id' => $this->config->get('flat198403_tax_class_id'),
				'text'         => $this->config->get('flat198403_cost')
			);

			$method_data = array(
				'code'       => 'flat198403',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('flat198403_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}