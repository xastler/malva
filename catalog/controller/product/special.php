<?php
class ControllerProductSpecial extends Controller {
	public function index() {
		$this->load->language('product/special');

		$this->load->model('catalog/product');

        $this->load->model('account/wishlist');

		$this->load->model('tool/image');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/special', $url)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
        $data['text_filter_btn'] = $this->language->get('text_filter_btn');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_continue'] = $this->language->get('button_continue');
        $data['text_ajax_pp'] = $this->language->get('text_ajax_pp');

		$data['compare'] = $this->url->link('product/compare');

		$data['products'] = array();

        if ($this->config->get('config_thumb_special')) {
            $data['thumb_special'] = $this->model_tool_image->myresize($this->config->get('config_thumb_special'), 755, 500);
            $this->document->setOgImage($data['thumb_special']);
        } else {
            $data['thumb_special'] = '';
        }

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);

		$product_total = $this->model_catalog_product->getTotalProductSpecials();

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		foreach ($results as $result) {
            $labels = array();
            if ($result['image']) {
                $image = $this->model_tool_image->myresize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            } else {
                $image = $this->model_tool_image->myresize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            }

            $results_image = $this->model_catalog_product->getProductImages($result['product_id']);
            if ($results_image) {

                $i=0;
                foreach  ($results_image as $result_image) {
                    if($i<1){
                        $image_hover = $this->model_tool_image->myresize($result_image['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                        $i++;
                    }
                }
            } elseif ($result['image']) {
                $image_hover = $this->model_tool_image->myresize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

            }else{
                $image_hover = $this->model_tool_image->myresize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            }

            if ($result['image_max']) {
                $image_max = $this->model_tool_image->myresize($result['image_max'], 555, 263);
            } elseif ($result['image']) {
                $image_max = $this->model_tool_image->myresize($result['image'],555, 263);
            } else {
                $image_max = $this->model_tool_image->myresize('placeholder.png', 555, 263);
            }
            if ($result['image_max_hover']) {
                $image_max_hover = $this->model_tool_image->myresize($result['image_max_hover'], 555, 263);
            } elseif ($result['image']) {
                $image_max_hover = $this->model_tool_image->myresize($result['image'],555, 263);
            } else {
                $image_max_hover = $this->model_tool_image->myresize('placeholder.png', 555, 263);
            }

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                $labels[] = array(
                    'text' => round(($result['special'] / $result['price'] * 100) - 100) . '%'
                );
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}

            $label_categories_id = array();

            $label_categories_id = $this->model_catalog_product->getCategoryByProductId($result['product_id'], $categories = array(63));
            if ($label_categories_id) {
                foreach ($label_categories_id as $label_category_id) {

                    $labels[] = array(
                        'text' => $result['label_text_array'][$label_category_id['category_id']]
                    );
                }

            }
            if ($this->customer->isLogged()) {
                $wishlist_status = false;
                if($this->model_account_wishlist->checkWishlistByProductId($result['product_id'])){
                    $wishlist_status = true;
                }
            }else {
                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = true;
                } else {
                    $wishlist_status = false;
                }
            }
			$data['products'][] = array(
				'product_id'  => $result['product_id'],
                'thumb' => $image,
                'thumb_hover' => $image_hover,
                'thumb_max' => $image_max,
                'thumb_max_hover' => $image_max_hover,
				'name'        => $result['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
                'labels' => $labels,
                'wishlist_status' => $wishlist_status,
				'tax'         => $tax,
				'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				'rating'      => $result['rating'],
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url)
			);
		}

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

        $data['sorts'] = array();

        $data['sorts'][] = array(
            'text' => $this->language->get('text_default'),
            'value' => 'p.sort_order-ASC',
            'href' => $this->url->link('product/special',  '&sort=p.sort_order&order=ASC' . $url)
        );
        $data['sorts'][] = array(
            'text' => $this->language->get('text_sort_news'),
            'value' => 'p.date_added-DESC',
            'href' => $this->url->link('product/special',  '&sort=p.date_added&order=DESC' . $url)
        );
        $data['sorts'][] = array(
            'text' => $this->language->get('text_sort_special'),
            'value' => 'p.sort_special-ASC',
            'href' => $this->url->link('product/special',  '&sort=p.sort_special&order=ASC' . $url)
        );

//			$data['sorts'][] = array(
//				'text'  => $this->language->get('text_name_asc'),
//				'value' => 'pd.name-ASC',
//				'href'  => $this->url->link('product/category',  '&sort=pd.name&order=ASC' . $url)
//			);
//
//			$data['sorts'][] = array(
//				'text'  => $this->language->get('text_name_desc'),
//				'value' => 'pd.name-DESC',
//				'href'  => $this->url->link('product/category',  '&sort=pd.name&order=DESC' . $url)
//			);

        $data['sorts'][] = array(
            'text' => $this->language->get('text_price_asc'),
            'value' => 'p.price-ASC',
            'href' => $this->url->link('product/special',  '&sort=p.price&order=ASC' . $url)
        );

        $data['sorts'][] = array(
            'text' => $this->language->get('text_price_desc'),
            'value' => 'p.price-DESC',
            'href' => $this->url->link('product/special',  '&sort=p.price&order=DESC' . $url)
        );

//			if ($this->config->get('config_review_status')) {
//				$data['sorts'][] = array(
//					'text'  => $this->language->get('text_rating_desc'),
//					'value' => 'rating-DESC',
//					'href'  => $this->url->link('product/category',  '&sort=rating&order=DESC' . $url)
//				);
//
//				$data['sorts'][] = array(
//					'text'  => $this->language->get('text_rating_asc'),
//					'value' => 'rating-ASC',
//					'href'  => $this->url->link('product/category',  '&sort=rating&order=ASC' . $url)
//				);
//			}
//
//			$data['sorts'][] = array(
//				'text'  => $this->language->get('text_model_asc'),
//				'value' => 'p.model-ASC',
//				'href'  => $this->url->link('product/category',  '&sort=p.model&order=ASC' . $url)
//			);
//
//			$data['sorts'][] = array(
//				'text'  => $this->language->get('text_model_desc'),
//				'value' => 'p.model-DESC',
//				'href'  => $this->url->link('product/category',  '&sort=p.model&order=DESC' . $url)
//			);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/special', $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/special', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));
        $data['pag_results'] = sprintf($this->language->get('text_pagination_p'), ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total);

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'canonical');
		} elseif ($page == 2) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'prev');
		} else {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page - 1), 'SSL'), 'prev');
		}

		if ($limit && ceil($product_total / $limit) > $page) {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page + 1), 'SSL'), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/special.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/special.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/special.tpl', $data));
		}
	}
}
