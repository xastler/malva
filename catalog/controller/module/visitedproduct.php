<?php
class ControllerModuleVisitedproduct extends Controller {
	public function index($setting) {
		$this->load->language('module/visitedproduct');
        $this->document->addStyle('catalog/view/javascript/jquery/slick-1.8.1/slick/slick.css');
        $this->document->addScript('catalog/view/javascript/jquery/slick-1.8.1/slick/slick.js');

		$data['heading_title'] = $this->language->get('heading_title');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_details'] = $this->language->get('button_details');

        $this->load->model('account/wishlist');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
		
		if(isset($this->session->data['products_id']))
			$this->session->data['products_id'] = array_slice($this->session->data['products_id'], -$setting['limit']);

		if (isset($this->session->data['products_id'])) {
			foreach ($this->session->data['products_id'] as $result_id) {
				$result = $this->model_catalog_product->getProduct($result_id);

                $labels = array();
                if ($result['image']) {
                    $image = $this->model_tool_image->myresize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->myresize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                $results_image = $this->model_catalog_product->getProductImages($result['product_id']);
                if ($results_image) {

                    $i=0;
                    foreach  ($results_image as $result_image) {
                        if($i<1){
                            $image_hover = $this->model_tool_image->myresize($result_image['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                            $i++;
                        }
                    }
                } elseif ($result['image']) {
                    $image_hover = $this->model_tool_image->myresize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

                }else{
                    $image_hover = $this->model_tool_image->myresize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if ($result['image_max']) {
                    $image_max = $this->model_tool_image->myresize($result['image_max'], 555, 263);
                } elseif ($result['image']) {
                    $image_max = $this->model_tool_image->myresize($result['image'],555, 263);
                } else {
                    $image_max = $this->model_tool_image->myresize('placeholder.png', 555, 263);
                }
                if ($result['image_max_hover']) {
                    $image_max_hover = $this->model_tool_image->myresize($result['image_max_hover'], 555, 263);
                } elseif ($result['image']) {
                    $image_max_hover = $this->model_tool_image->myresize($result['image'],555, 263);
                } else {
                    $image_max_hover = $this->model_tool_image->myresize('placeholder.png', 555, 263);
                }
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				//var_dump($price);
				

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    $labels[] = array(
                        'text' => round(($result['special'] / $result['price'] * 100) - 100) . '%'
                    );
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				if(utf8_strlen($result['name']) < 80){
					$sub_name = $result['name'];
				}else{
					$sub_name = utf8_substr($result['name'], 0, 80) . '..';
				}

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                $label_categories_id = array();

                $label_categories_id = $this->model_catalog_product->getCategoryByProductId($result['product_id'], $categories = array(63));
                if ($label_categories_id) {
                    foreach ($label_categories_id as $label_category_id) {

                        $labels[] = array(
                            'text' => $result['label_text_array'][$label_category_id['category_id']]
                        );
                    }

                }

                if ($this->customer->isLogged()) {
                    $wishlist_status = false;
                    if($this->model_account_wishlist->checkWishlistByProductId($result['product_id'])){
                        $wishlist_status = true;
                    }
                }else {
                    if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                        $wishlist_status = true;
                    } else {
                        $wishlist_status = false;
                    }
                }
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
                    'thumb' => $image,
                    'thumb_hover' => $image_hover,
                    'thumb_max' => $image_max,
                    'thumb_max_hover' => $image_max_hover,
					'name'        => $sub_name ,
					'wishlist_status' => $wishlist_status ,
					'compare_status' => $compare_status ,
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
                    'labels' => $labels,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/visitedproduct.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/visitedproduct.tpl', $data);
        } else {
            return $this->load->view('default/template/module/special.tpl', $data);
        }
	}
}