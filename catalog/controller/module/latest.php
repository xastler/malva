<?php
class ControllerModuleLatest extends Controller {
	public function index($setting) {
		$this->load->language('module/latest');
        $this->load->model('account/wishlist');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_btn'] = $this->language->get('text_btn');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
        $data['page_new'] = $this->url->link('product/category', 'category_id=' . 63);

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        $filter_data = array(
            'filter_category_id' => 63
        );

        $data['products'] = array();

        $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                $labels = array();

                if ($result['image']) {
                    $image = $this->model_tool_image->myresize($result['image'], 262.5, 263);
                } else {
                    $image = $this->model_tool_image->myresize('placeholder.png', 262.5, 263);
                }

                $results_image = $this->model_catalog_product->getProductImages($result['product_id']);
                if ($results_image) {

                    $i=0;
                    foreach  ($results_image as $result_image) {
                        if($i<1){
                            $image_hover = $this->model_tool_image->myresize($result_image['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                            $i++;
                        }
                    }
                } elseif ($result['image']) {
                    $image_hover = $this->model_tool_image->myresize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

                }else{
                    $image_hover = $this->model_tool_image->myresize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if ($result['image_max']) {
                    $image_max = $this->model_tool_image->myresize($result['image_max'], 555, 263);
                } elseif ($result['image']) {
                    $image_max = $this->model_tool_image->myresize($result['image'],555, 263);
                } else {
                    $image_max = $this->model_tool_image->myresize('placeholder.png', 555, 263);
                }
                if ($result['image_max_hover']) {
                    $image_max_hover = $this->model_tool_image->myresize($result['image_max_hover'], 555, 263);
                } elseif ($result['image']) {
                    $image_max_hover = $this->model_tool_image->myresize($result['image'],555, 263);
                } else {
                    $image_max_hover = $this->model_tool_image->myresize('placeholder.png', 555, 263);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));

                    $labels[] = array(
                        'text' => round(($result['special']/$result['price']*100)-100) . '%'
                    );


                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }

                $label_categories_id = $this->model_catalog_product->getCategoryByProductId($result['product_id'], $categories = array(63) );

                foreach ($label_categories_id as $label_category_id) {

                    $labels[] = array(
                        'text' => $result['label_text_array'][$label_category_id['category_id']]
                    );
                }

                if ($this->customer->isLogged()) {
                    $wishlist_status = false;
                    if($this->model_account_wishlist->checkWishlistByProductId($result['product_id'])){
                        $wishlist_status = true;
                    }
                }else {
                    if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                        $wishlist_status = true;
                    } else {
                        $wishlist_status = false;
                    }
                }

                $data['products'][] = array(
                    'product_id'   => $result['product_id'],
                    'thumb' => $image,
                    'thumb_hover' => $image_hover,
                    'thumb_max' => $image_max,
                    'thumb_max_hover' => $image_max_hover,
                    'name'         => $result['name'],
                    'description'  => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'        => $price,
                    'special'      => $special,
                    'labels'       => $labels,
                    'wishlist_status' => $wishlist_status,
                    'tax'          => $tax,
                    'rating'       => $rating,
                    'href'         => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }

//		if ($results) {
//			foreach ($results as $result) {
//				if ($result['image']) {
//					$image = $this->model_tool_image->resize($result['image'], 262.5, 263);
//				} else {
//					$image = $this->model_tool_image->resize('placeholder.png', 262.5, 263);
//				}
//				if ($result['image_max']) {
//					$image_max = $this->model_tool_image->resize($result['image_max'], 555, 263);
//				} else {
//					$image_max = $this->model_tool_image->resize('placeholder.png',  555, 263);
//				}
//
//				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
//					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
//				} else {
//					$price = false;
//				}
//
//				if ((float)$result['special']) {
//					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
//				} else {
//					$special = false;
//				}
//
//				if ($this->config->get('config_tax')) {
//					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
//				} else {
//					$tax = false;
//				}
//
//				if ($this->config->get('config_review_status')) {
//					$rating = $result['rating'];
//				} else {
//					$rating = false;
//				}
//
//				$data['products'][] = array(
//					'product_id'  => $result['product_id'],
//					'thumb'       => $image,
//                    'thumb_max'   => $image_max,
//					'name'        => $result['name'],
//					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
//					'price'       => $price,
//					'special'     => $special,
//					'tax'         => $tax,
//					'rating'      => $rating,
//					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
//				);
//			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latest.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/latest.tpl', $data);
			} else {
				return $this->load->view('default/template/module/latest.tpl', $data);
			}
//		}
	}
}
