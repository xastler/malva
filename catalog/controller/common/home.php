<?php
class ControllerCommonHome extends Controller {
	public function index() {

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

         $this->config->get('config_meta_title');

        $this->load->language('common/home');

        $data['text_baner_plus_1'] = $this->language->get('text_baner_plus_1');
        $data['text_baner_plus_2'] = $this->language->get('text_baner_plus_2');
        $data['text_baner_plus_3'] = $this->language->get('text_baner_plus_3');
        $data['text_baner_plus_4'] = $this->language->get('text_baner_plus_4');
        $data['text_open'] = $this->language->get('text_open');

        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);
        $k=1;
        foreach ($categories as $category) {
            if ($k<=4) {
                $k++;
                // Level 1
                if ($category['image']) {
                    $image = $this->model_tool_image->resize($category['image'],  407, 500);
                } else {
                    $image = $this->model_tool_image->resize('no_image.png' , 407, 500);
                }
                $data['categories'][] = array(
                    'name'          => $category['name'],
                    'description'   => $category['description'],
                    'image'         => $image,
                    'href'          => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

		if (isset($this->request->get['route'])) {
			$this->document->addLink(((isset($this->request->server['HTTPS']) && !empty($this->request->server['HTTPS'])) ? HTTPS_SERVER : HTTP_SERVER), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
